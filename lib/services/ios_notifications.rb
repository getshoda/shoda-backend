require 'yaml'
require 'pushmeup'

module Services
  class IOSNotifications
    PRODUCTION = 'prod'

    def initialize(env = 'dev')

      env_key = env == PRODUCTION ? :production : :sandbox
      APNS.host = CONFIGURATION.aps.send(env_key).host
      APNS.port = CONFIGURATION.aps.send(env_key).port
      APNS.pass = CONFIGURATION.aps.send(env_key).password
      APNS.pem  = CONFIGURATION.aps.send(env_key).pem_path
    end

    def send_notification(device_token, message)
      APNS.send_notification(
        device_token,
        alert: message.title,
        badge: 1,
        sound: 'default',
        other: { campaign_id: message.campaign_id, message_id: message.id }
      )
    end
  end
end
