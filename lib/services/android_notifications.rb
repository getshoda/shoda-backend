require 'yaml'
require 'pushmeup'

module Services
  class AndroidNotifications

    def initialize
      GCM.host = CONFIGURATION.gcm.host
      GCM.key = CONFIGURATION.gcm.key
    end

    def send_notification(device_token, message)
      GCM.send_notification(
        device_token,
        title: message.title,
        body: message.body,
        campaign_id: message.campaign_id,
        message_id: message.id
      )
    end
  end
end