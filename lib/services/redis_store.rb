require 'redis'
require 'singleton'

module Services
  class RedisStore
    include Singleton

    def initialize
      @connection ||= Redis.new(CONFIGURATION.redis.to_h)
    end

    def get(namespace, key)
      @connection.get(absolute_key(namespace, key))
    end

    def set(namespace, key, value, ttl=nil)
      if ttl
        @connection.setex(absolute_key(namespace, key), ttl, value)
      else
        @connection.set(absolute_key(namespace, key), value)
      end
    end

    def del(namespace, key)
      @connection.del(absolute_key(namespace, key))
    end

    private
    def absolute_key(namespace, key)
      namespace ? "#{namespace}:#{key}" : key
    end
  end
end