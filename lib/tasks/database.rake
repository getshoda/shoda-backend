require 'erb'
require 'yaml'
require 'active_record'

namespace :db do

  def config
    YAML.load(ERB.new(File.read(File.join('config', 'database.yml'))).result)[DATABASE_ENV]
  end

  task :environment do
    DATABASE_ENV = ENV['RACK_ENV'] || 'development'
  end

  task :configure_connection => :environment do
    ActiveRecord::Base.establish_connection(config)
  end

  desc 'Runs the migrations'
  task :migrate => :configure_connection do
    ActiveRecord::Migration.verbose = true
    ActiveRecord::Migrator.migrate 'db/migrations'

    Rake::Task['db:dump_schema'].invoke if ActiveRecord::Base.schema_format == :ruby
  end

  desc 'Creates the database'
  task :create => :environment do
    ActiveRecord::Base.establish_connection(config.merge('database' => nil))
    ActiveRecord::Base.connection.create_database(config['database']) 
  end

  desc 'Drops the database'
  task :drop => :configure_connection do
    ActiveRecord::Base.connection.drop_database(config['database'])
  end
  
  task :reset => ['db:drop', 'db:create', 'db:migrate']

  desc 'Create a db/schema.rb file with the db schema'
  task :dump_schema => [:configure_connection] do
    require 'active_record/schema_dumper'
    
    filename = ENV['SCHEMA'] || File.join('db', 'schema.rb')

    File.open(filename, 'w:utf-8') do |file|
      ActiveRecord::SchemaDumper.dump(ActiveRecord::Base.connection, file)
    end
  end
end