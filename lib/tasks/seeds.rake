require 'spec/factories/interest'
require 'spec/factories/enterprise'

namespace :chop do

  desc 'Run the task and chop chop'
  task :chop do
    require 'config/boot'
    interests_names = %w(clothes food sports dr_who batman)

    interests_names.each do |name|
      FactoryGirl.create(:interest, name: name)
    end

    enterprises_names = %w(ACME ApertureLabs UmbrellaCorp WayneIndustries StarkIndustries TheDailyBuggle)

    enterprises_names.each do |name|
      FactoryGirl.create(:enterprise, name: name)
    end
  end
end
