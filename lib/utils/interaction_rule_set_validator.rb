require 'lib/utils/interaction_rule'

module Utils
  module InteractionRuleSetValidator
    
    def self.validate!(rule_set)
      return if rule_set.empty?

      rule_set.each { |rule| validate_rule(rule) }

      nil
    end

    def self.validate_rule(rule)
      attributes = rule[:attributes] || rule['attributes'] 
      validate_rule_attributes(attributes)

      operator = rule[:operator] || rule['operator']
      validate_rule_operator(operator)
    end
    private_class_method :validate_rule

    def self.validate_rule_attributes(attributes)
      raise EmptyRuleAttributesError if attributes.nil? || attributes.empty?

      allowed_interaction = InteractionRule.allowed_interaction_attributes
      
      attributes.inject(allowed_interaction) do |subject, attribute| 
        raise InvalidRuleAttribute.new(attribute) unless subject.respond_to?(attribute)

        subject.send(attribute)
      end
    end
    private_class_method :validate_rule_attributes

    def self.validate_rule_operator(operator)
      raise BlankRuleOperator if operator.nil?
      
      unless InteractionRule.allowed_operators.include?(operator.to_sym)
        raise InvalidRuleOperator.new(operator) 
      end
    end
  end
end

class InvalidRuleAttribute < StandardError
  def initialize(attr_name = nil)
    @attr_name = attr_name
  end

  def message
    "Invalid attribute #@attr_name"
  end
end

class EmptyRuleAttributesError < StandardError
  def message
    'The rule attributes cannot be empty.'
  end
end

class InvalidRuleOperator < StandardError
  def initialize(operator_name)
    @operator_name = operator_name
  end

  def message
    "Invalid operator #@operator_name"
  end
end

class BlankRuleOperator < StandardError
  def message
    'The rule must have an operator.'
  end
end