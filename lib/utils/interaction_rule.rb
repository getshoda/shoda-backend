require 'ostruct'

module Utils
  class InteractionRule
    attr_reader :subject, :operator, :values

    def initialize(interaction, attributes, operator, values)
      @subject = interaction.send_chain(attributes)
      @operator = operator
      @values = values
    end

    def eval
      self.send(operator)
    end

    def self.allowed_operators
      [:eq, :includes, :between, :lesser, :greater]
    end

    def self.allowed_interaction_attributes
      OpenStruct.new(
        user: OpenStruct.new(
          age: rand(1_000),
          interest_names: [],
          gender: '',
          first_name: '',
          last_name: ''
        )
      )
    end

    private

    def eq(op = values.first)
      subject == op
    end

    def includes(op = values.first)
      subject.include?(op)
    end

    def between(ops = values)
      min, max = ops

      greater(min) && lesser(max)
    end

    def lesser(op = values.first)
      return true if op.nil? || subject.nil?

      subject < op
    end

    def greater(op = values.first)
      return true if op.nil? || subject.nil?

      subject > op
    end
  end
end
