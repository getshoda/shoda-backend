module SharedParams
  extend Grape::API::Helpers

  params :pagination do
    optional :page, type: Integer
    optional :per_page, type: Integer
  end

  # Loads all the models and based on the options[:resource] options it uses that model
  #   to find with columns are allowed and builds the query parameters.
  params :query do |options|
    Dir['models/*.rb'].each { |file| require file }

    resource = Object.const_get(options[:resource].to_s.camelize) #BLACK MAGIC

    resource.allowed_columns.each do |column|
      optional "#{column}__in"
      optional "#{column}__like"
      optional "#{column}__eq"
      optional "#{column}__null", type: String, values: %w(true false)
    end
  end
end
