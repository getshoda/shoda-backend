require 'lib/utils/interaction_rule'

module Utils
  class InteractionRulesSet
    attr_reader :interaction, :rules

    def initialize(interaction, rules)
      @interaction = interaction
      @rules = rules
    end

    def eval      
      rules.all? do |rule|
        Utils::InteractionRule.new(
          interaction, 
          rule[:attributes] || rule['attributes'], 
          rule[:operator] || rule['operator'], 
          rule[:values] || rule['values']
        ).eval
      end
    end
  end
end