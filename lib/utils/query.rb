require 'active_support'

require 'exceptions/query_errors'

module Utils
  module Query
    extend ActiveSupport::Concern

    DEFAULT_PER_PAGE = 15

    module ClassMethods
      def query_relation
        self
      end

      def set_queryable_columns(*args)
        @oswald_allowed_columns = args
      end

      def allowed_columns
        @oswald_allowed_columns || []
      end

      def paginate(paging = {})
        per_page = paging[:per_page] || DEFAULT_PER_PAGE
        page = paging[:page] || 1

        query_relation.limit(per_page).offset((page - 1) * per_page)
      end

      def filter(filters = {})
        query, values = build_filter(filters)

        query_relation.where(query, *values)
      end

      private
      def build_filter(filters = {})
        query_elements = []
        values = []

        filters.each do |key, value|
          attribute, operation = key.to_s.split('__')

          validate_attribute!(attribute)

          query_element, query_value = to_sql(attribute, operation, value)

          query_elements << query_element
          values << query_value if query_value
        end

        return query_elements.join(' and '), values
      end

      def to_sql(attribute, operation, value)
        case operation
        when 'like'
          return "#{attribute} ilike ?", "%#{value}%"
        when 'in'
          return "#{attribute} in (?)", value
        when 'eq'
          return "#{attribute} = ?", value
        when 'null'
          "#{attribute} is #{'not ' if value == 'false'}null"
        else
          raise InvalidOperation.new(operation)
        end
      end

      def validate_attribute!(attribute)
        allowed_columns.include?(attribute.to_sym) or raise InvalidAttribute.new(attribute)
      end
    end
  end
end
