module Business
  class Devices

    def create(user_id, attrs)
      attrs.merge!(:user_id => user_id)
      
      Device.find_or_create_by!(attrs)
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end
  end
end