module Business
  class Branches
    def find(id)
      Branch.find(id)
    rescue ActiveRecord::RecordNotFound
      raise BranchNotFound.new(id)
    end

    def get(q_params = {})
      page = q_params.delete('page')
      per_page = q_params.delete('per_page')

      Branch.filter(q_params).paginate(per_page: per_page, page: page)
    end

    def create(attrs)
      Branch.create!(attrs)
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    def update(id, attrs)
      branch = find(id)

      branch.update_attributes!(attrs)

      branch
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    def delete(id)
      find(id).destroy
    end

    def by_campaign(campaing_id)
      campaign = campaigns_business.find(campaing_id)

      campaign.branches
    end

    private

    def campaigns_business
      @campaigns_business ||= Business::Campaigns.new
    end
  end
end