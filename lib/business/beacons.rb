module Business
  class Beacons

    def find(id)
      Beacon.find(id)
    rescue ActiveRecord::RecordNotFound
      raise BeaconNotFound.new(id)
    end

    def get(q_params = {})
      page = q_params.delete('page')
      per_page = q_params.delete('per_page')

      Beacon.filter(q_params).paginate(per_page: per_page, page: page)
    end

    def create(attrs)
      Beacon.create!(attrs)
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    def update(id, attrs)
      beacon = find(id)

      beacon.update_attributes!(attrs)

      beacon
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    def discover(major, minor, uuid)
      Beacon.discover(major, minor, uuid) or raise BeaconNotFound.new(uuid)
    end

    def delete(id)
      find(id).destroy
    end
  end
end