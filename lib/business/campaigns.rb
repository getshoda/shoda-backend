module Business
  class Campaigns

    def create(attrs)
      format_attrs(attrs)

      Campaign.create!(attrs)
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    def get(q_params = {})
      page = q_params.delete('page')
      per_page = q_params.delete('per_page')

      Campaign.filter(q_params).paginate(per_page: per_page, page: page)
    end

    def find(id)
      Campaign.find(id)
    rescue ActiveRecord::RecordNotFound
      raise CampaignNotFound.new(id)
    end

    def delete(id)
      find(id).destroy
    end

    def update(id, attrs)
      campaign = find(id)

      format_attrs(attrs)

      campaign.update_attributes!(attrs)

      campaign
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    private

    def branches_business
      @branches_business ||= Business::Branches.new
    end

     def format_attrs(attrs)
      branches_ids = attrs.delete('branches_ids') || []

      branches = branches_ids.map { |id| branches_business.find(id) }
      attrs['branches'] = branches
    end

  end
end
