require 'lib/business/interests'

module Business
  class Users

    def find(id)
      User.find(id)
    rescue ActiveRecord::RecordNotFound
      raise UserNotFound.new(id)
    end

    def get
      User.all
    end

    def create(attrs)
      User.create!(attrs)
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    rescue ActiveRecord::RecordNotUnique => e
      raise EmailIsNotUnique
    end

    def update(id, attrs)
      user = find(id)

      format_interests(attrs)
      user.update_attributes!(attrs)

      user
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    rescue ActiveRecord::RecordNotUnique => e
      raise EmailIsNotUnique
    end

    def login_with_facebook(facebook_id, token)
      user = User.find_by_facebook_id(facebook_id)

      user_attrs = facebook_user(token)
      return update(user.id, facebook_token: token) unless user.nil?

      create(
        first_name: user_attrs['first_name'],
        last_name: user_attrs['last_name'],
        gender: user_attrs['gender'],
        locale: user_attrs['locale'],
        facebook_id: facebook_id,
        facebook_token: token
      )
    end

    def login_with_email(email, password)
      user = User.find_by_email(email)

      raise InvalidUser unless user && user.authenticate(password)

      user
    end

    def login_with_twitter(token, secret)
      twitter_info = twitter_user(token, secret)

      user = User.find_by_twitter_id(twitter_info.id.to_s)
      return update(user.id, twitter_token: token) unless user.nil?

      create(
        username: twitter_info.screen_name,
        twitter_id: twitter_info.id,
        twitter_token: token
      )
    end

    private
    def facebook_user(token)
      Koala::Facebook::API.new(token).get_object('me')
    rescue Koala::Facebook::AuthenticationError
      raise InvalidUser
    end

    def format_interests(attrs)
      interests_ids = attrs.delete('interests_ids')
      return unless interests_ids

      attrs[:interests] = interests_ids.map do |interest_id|
        Business::Interests.new.find(interest_id)
      end
    end

    def twitter_user(token, secret)
      twitter_client(token, secret).user
    rescue Twitter::Error::BadRequest
      raise InvalidUser
    end

    def twitter_client(token, secret)
      Twitter::REST::Client.new do |config|
        config.consumer_key        = CONFIGURATION.twitter.consumer_key
        config.consumer_secret     = CONFIGURATION.twitter.consumer_secret
        config.access_token        = token
        config.access_token_secret = secret
      end
    end
  end
end