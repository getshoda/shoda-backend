require 'lib/utils/interaction_rules_set'

module Business
  class Messages

    def find(id)
      Message.find(id)
    rescue ActiveRecord::RecordNotFound
      raise MessageNotFound.new(id)
    end

    def get(campaign_id)
      Message.rewhere(campaign_id: campaign_id)
    end

    def create(attrs)
      beacons_ids = attrs.delete('beacons_ids') || []

      beacons = beacons_ids.map { |id| beacons_business.find(id) }
      attrs['beacons'] = beacons

      Message.create!(attrs)
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    def update(id, attrs)
      message = find(id)

      message.update_attributes!(attrs)

      message
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    def delete(id)
      find(id).destroy
    end

    def get_by_context(interaction)
      messages = if interaction.is_a_new_visit?
        interaction.update_attribute(:new_visit, true)

        Message.welcome_by_enterprise(interaction.beacon.branch.enterprise_id)
      end

      messages = interaction.beacon.messages if messages.nil? || messages.empty?

      messages.select do |message|
        message.enabled? &&
          Utils::InteractionRulesSet.new(interaction, message.rules).eval
      end.sample
    end

    private
    def users_business
      @users_business ||= Business::Users.new
    end

    def beacons_business
      @beacons_business ||= Business::Beacons.new
    end
  end
end
