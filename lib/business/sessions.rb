require 'jwt'

require 'lib/business/users'
require 'lib/services/redis_store'

module Business
  class Sessions

    USER_ID_NAMESPACE = 'auth:user_id'
    TOKEN_NAMESPACE = 'auth:token'

    ACCESS_TOKEN_TTL = 86400 # 60 * 60 * 24

    def get_session(user)
      redis_instance.get(USER_ID_NAMESPACE, user.id) || create_session(user) if user
    end

    def destroy_session(token)
      user_id = redis_instance.get(TOKEN_NAMESPACE, token)

      redis_instance.del(TOKEN_NAMESPACE, token)
      redis_instance.del(USER_ID_NAMESPACE, user_id)
    end

    def find_user_with_session(token)
      user_id = redis_instance.get(TOKEN_NAMESPACE, token)

      users_business.find(user_id)
    end

    private
    def create_session(user)
      expires = Time.now.to_i + ACCESS_TOKEN_TTL

      JWT.encode({ iss: "#{user.id}", exp: expires }, secret_for_user(user)).tap do |token|
        redis_instance.set(USER_ID_NAMESPACE, user.id, token, ACCESS_TOKEN_TTL)
        redis_instance.set(TOKEN_NAMESPACE, token, user.id, ACCESS_TOKEN_TTL)
      end
    end

    def secret_for_user(user)
      "#{user.salt}$#{CONFIGURATION.authentication.secret}"
    end

    def users_business
      @users_business ||= Business::Users.new
    end

    def redis_instance
      Services::RedisStore.instance
    end
  end
end
