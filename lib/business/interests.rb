require 'exceptions/interest_error'

module Business
  class Interests
    def find(id)
      Interest.find_by_id(id) or raise InterestNotFound.new(id)
    end

    def get
      Interest.all
    end
  end
end