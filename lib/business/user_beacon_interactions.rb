module Business
  class UserBeaconInteractions

    def get(user_id, q_params)
      page = q_params.delete('page')
      per_page = q_params.delete('per_page')

      UserBeaconInteraction
        .where(user_id: user_id)
        .filter(q_params)
        .paginate(per_page: per_page, page: page)
    end

    def create(user_id, attrs)
      beacon = beacons_business.discover(
        attrs.delete('major'),
        attrs.delete('minor'),
        attrs.delete('uuid')
      )

      attrs['at'] ||= Time.now

      attrs.merge!(user_id: user_id, beacon_id: beacon.id)

      interaction = UserBeaconInteraction.create!(attrs)

      nofity_user_if_applies(interaction).tap do |message|
        interaction.update_attributes!(message_id: message.id) unless message.nil?
      end

      interaction
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    private
    def nofity_user_if_applies(interaction)
      return unless can_be_notified?(interaction.user_id)

      message = messages_business.get_by_context(interaction)

      if message
        user = users_business.find(interaction.user_id)

        user.devices.each { |device| device.notify(message) }
      end

      message
    end

    def can_be_notified?(user_id)
      now = Time.now
      last_message_interaction = UserBeaconInteraction.last_message_for_user(user_id)

      return true unless last_message_interaction

      now - last_message_interaction.at >= CONFIGURATION.interval_between_notifications
    end

    def messages_business
      @messages_business ||= Business::Messages.new
    end

    def beacons_business
      @beacons_business ||= Business::Beacons.new
    end

    def users_business
      @users_business ||= Business::Users.new
    end
  end
end
