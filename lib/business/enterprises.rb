module Business
  class Enterprises
    def find(id)
      Enterprise.find(id)
    rescue ActiveRecord::RecordNotFound
      raise EnterpriseNotFound.new(id)
    end

    def get(q_params)
      page = q_params.delete('page')
      per_page = q_params.delete('per_page')

      Enterprise.filter(q_params).paginate(per_page: per_page, page: page)
    end

    def create(attrs)
      Enterprise.create!(attrs)
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    def update(id, attrs)
      enterprise = find(id)

      enterprise.update_attributes!(attrs)

      enterprise
    rescue ActiveRecord::RecordInvalid => e
      raise ResourceInvalid.new(e.record.errors)
    end

    def delete(id)
      find(id).destroy
    end
  end
end