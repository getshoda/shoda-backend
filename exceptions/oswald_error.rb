class OswaldError < StandardError
  def key
    self.class.name
  end

  def status
    raise NotImplementedError
  end

  def message
    raise NotImplementedError
  end

  def to_json
    {
      status:  status,
      error:   key,
      message: message
    }.to_json
  end
end
