require_relative 'oswald_error'

class BeaconNotFound < OswaldError
  def initialize(id)
    @id = id
  end

  def message
    "Beacon not found for the given id (#@id)"
  end

  def status
    404
  end
end