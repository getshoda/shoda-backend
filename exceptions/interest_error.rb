require_relative 'oswald_error'

class InterestNotFound < OswaldError
  def initialize(id)
    @id = id
  end

  def message
    "Interest not found for the given id (#@id)"
  end

  def status
    404
  end
end