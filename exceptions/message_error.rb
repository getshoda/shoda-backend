require_relative 'oswald_error'

class MessageNotFound < OswaldError
  def initialize(id)
    @id = id
  end

  def message
    "Message not found for the given id (#@id)"
  end

  def status
    404
  end
end