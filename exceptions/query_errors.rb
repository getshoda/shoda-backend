require_relative 'oswald_error'

class InvalidAttribute < OswaldError
  def initialize(attribute)
    @attribute = attribute
  end

  def message
    "#@attribute is not a valid attribute for the resource."
  end

  def status
    422
  end
end

class InvalidOperation < OswaldError
  def initialize(operation)
    @operation = operation
  end

  def message
    "#@operation is not a valid query operation."
  end

  def status
    422
  end
end