require_relative 'oswald_error'

class CampaignNotFound < OswaldError
  def initialize(id)
    @id = id
  end

  def message
    "Campaign not found for the given id (#@id)"
  end

  def status
    404
  end
end