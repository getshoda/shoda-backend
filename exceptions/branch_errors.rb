require_relative 'oswald_error'

class BranchNotFound < OswaldError
  def initialize(id)
    @id = id
  end

  def message
    "Branch not found for the given id (#@id)"
  end

  def status
    404
  end
end