require_relative 'oswald_error'

class EnterpriseNotFound < OswaldError
  def initialize(id)
    @id = id
  end

  def message
    "Enterprise not found for the given id (#@id)"
  end

  def status
    404
  end
end