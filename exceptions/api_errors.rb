require_relative 'oswald_error'

class MissingParameter < OswaldError
  def initialize(*missing_params)
    @missing_params = missing_params
  end

  def message
    "Required parameters (#{@missing_params.join(', ')}) are missing"
  end

  def status
    422
  end
end

class ResourceInvalid < OswaldError
  attr_reader :message

  def initialize(error_messages)
    @message = error_messages.values.flatten.join(', ')
  end

  def status
    422
  end
end