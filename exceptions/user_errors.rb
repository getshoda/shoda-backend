require_relative 'oswald_error'

class UserNotFound < OswaldError
  def initialize(id)
    @id = id
  end

  def message
    "User not found for the given id (#@id)"
  end

  def status
    404
  end
end

class InvalidUser <  OswaldError
  def message
    'Invalid user'
  end

  def status
    401
  end
end

class EmailIsNotUnique < OswaldError
  def message
    'A user with the same email address already exists'
  end

  def status
    409
  end
end