class AddOfflineFlagToCampaigns < ActiveRecord::Migration

  def up
    add_column :campaigns, :offline, :bool, default: false
  end

  def down
    remove_column :campaigns, :offline
  end

end
