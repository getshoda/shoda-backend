class CreateInterestsUsers < ActiveRecord::Migration
  def self.up
    create_table :interests_users do |t|
      t.belongs_to :user
      t.belongs_to :interest
    end
  end
 
  def self.down
    drop_table :interests_users
  end
end