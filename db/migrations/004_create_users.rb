class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string    :first_name
      t.string    :last_name
      t.string    :email
      t.string    :gender
      t.date      :birth_date
      t.string    :locale
      t.string    :facebook_id
      t.string    :facebook_token

      t.timestamps
    end
  end
 
  def self.down
    drop_table :users
  end
end