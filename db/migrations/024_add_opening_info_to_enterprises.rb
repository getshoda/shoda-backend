class AddOpeningInfoToEnterprises < ActiveRecord::Migration

  def up
    add_column :enterprises, :open_from, :string
    add_column :enterprises, :open_to, :string
  end

  def down
    remove_column :enterprises, :open_from
    remove_column :enterprises, :open_to
  end

end