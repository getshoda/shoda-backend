class CreateDevices < ActiveRecord::Migration
  def self.up
    create_table :devices do |t|
      t.string     :token
      t.string     :platform
      t.string     :version
      t.string     :environment
      t.belongs_to :user
    end
  end
 
  def self.down
    drop_table :users
  end
end