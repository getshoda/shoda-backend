class CreateBranchesCampaigns < ActiveRecord::Migration
  def self.up
    create_table :branches_campaigns, id: false do |t|
      t.belongs_to :branch
      t.belongs_to :campaign
    end
  end
 
  def self.down
    drop_table :branches_campaigns
  end
end