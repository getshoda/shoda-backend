class AddContentToMessage < ActiveRecord::Migration
  def up
    add_column :messages, :content, :string, default: {}.to_yaml
  end
end