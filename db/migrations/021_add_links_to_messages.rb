class AddLinksToMessages < ActiveRecord::Migration

  def up
    add_column :messages, :image_url, :string
    add_column :messages, :video_url, :string
  end

  def down
    remove_column :messages, :image_url
    remove_column :messages, :video_url
  end

end
