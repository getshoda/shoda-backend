class AddContactInfoToEnterprises < ActiveRecord::Migration

  def up
    add_column :enterprises, :website, :string
    add_column :enterprises, :logo_url, :string
    add_column :enterprises, :phone, :string
  end

  def down
    remove_column :enterprises, :website
    remove_column :enterprises, :logo_url
    remove_column :enterprises, :phone
  end

end