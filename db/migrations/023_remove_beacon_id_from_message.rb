class RemoveBeaconIdFromMessage < ActiveRecord::Migration
  def change
    remove_column :messages, :beacon_id
  end
end