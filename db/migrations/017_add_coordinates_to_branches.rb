class AddCoordinatesToBranches < ActiveRecord::Migration

  def up
    add_column :branches, :latitude, :float
    add_column :branches, :longitude, :float
  end

  def down
    remove_column :branches, :latitude
    remove_column :branches, :longitude
  end

end