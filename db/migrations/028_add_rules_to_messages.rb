class AddRulesToMessages < ActiveRecord::Migration

  def up
    add_column :messages, :rules, :string, default: [].to_yaml
  end

  def down
    remove_column :messages, :rules
  end
end