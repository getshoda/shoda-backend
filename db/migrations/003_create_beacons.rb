class CreateBeacons < ActiveRecord::Migration
  def self.up
    create_table :beacons do |t|
      t.string      :uuid
      t.integer     :major
      t.integer     :minor
      t.string      :name
      t.string      :string_id
      t.belongs_to  :branch
    end
  end
 
  def self.down
    drop_table :beacons
  end
end