class AddNewVisitToInteractions < ActiveRecord::Migration

  def up
    add_column :user_beacon_interactions, :new_visit, :bool, default: false
  end

  def down
    remove_column :user_beacon_interactions, :new_visit
  end

end
