class AddEnterpriseToCampaigns < ActiveRecord::Migration
  def up
    add_reference :campaigns, :enterprise, index: true, foreign_key: true
  end
end