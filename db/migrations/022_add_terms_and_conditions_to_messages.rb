class AddTermsAndConditionsToMessages < ActiveRecord::Migration

  def up
    add_column :messages, :terms_and_conditions, :string
  end

  def down
    remove_column :messages, :terms_and_conditions
  end
end