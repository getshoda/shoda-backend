class CreateCampaigns < ActiveRecord::Migration
  def self.up
    create_table :campaigns do |t|
      t.string                   :title
      t.date                     :from
      t.date                     :to
    end
  end
 
  def self.down
    drop_table :campaigns
  end
end