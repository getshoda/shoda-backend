class AddWebsiteToBranches < ActiveRecord::Migration

  def up
    add_column :branches, :website, :string
  end

  def down
    remove_column :branches, :website
  end

end