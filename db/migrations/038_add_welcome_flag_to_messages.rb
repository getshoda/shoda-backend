class AddWelcomeFlagToMessages < ActiveRecord::Migration

  def up
    add_column :messages, :welcome, :bool, default: false
  end

  def down
    remove_column :messages, :welcome
  end

end
