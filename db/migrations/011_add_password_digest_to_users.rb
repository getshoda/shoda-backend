class AddPasswordDigestToUsers < ActiveRecord::Migration

  def change
    add_column :users, :password_digest, :string
    add_column :users, :has_password, :bool, default: false
  end

end