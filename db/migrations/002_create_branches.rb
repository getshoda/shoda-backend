class CreateBranches < ActiveRecord::Migration
  def self.up
    create_table :branches do |t|
      t.string     :name
      t.text       :description
      t.text       :address
      t.belongs_to :enterprise
    end
  end
 
  def self.down  
    drop_table :branches
  end
end