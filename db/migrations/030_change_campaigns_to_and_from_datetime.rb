class ChangeCampaignsToAndFromDatetime < ActiveRecord::Migration

  def up
    change_column :campaigns, :to, :datetime
    change_column :campaigns, :from, :datetime
  end

  def down
    change_column :campaigns, :to, :date
    change_column :campaigns, :from, :date
  end

end