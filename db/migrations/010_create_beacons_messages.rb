class CreateBeaconsMessages < ActiveRecord::Migration
  def self.up
    create_table :beacons_messages, id: false do |t|
      t.belongs_to :beacon
      t.belongs_to :message
    end
  end
 
  def self.down
    drop_table :beacons_messages
  end
end