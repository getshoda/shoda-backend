class AddTimestampsToInteractions < ActiveRecord::Migration
  def up
    add_column :user_beacon_interactions, :created_at, :datetime
    add_column :user_beacon_interactions, :updated_at, :datetime
  end
end