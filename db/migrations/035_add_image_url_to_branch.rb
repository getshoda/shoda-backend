class AddImageUrlToBranch < ActiveRecord::Migration
  def up
    add_column :branches, :image_url, :string
  end
end