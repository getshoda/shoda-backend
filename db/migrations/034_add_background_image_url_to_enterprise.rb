class AddBackgroundImageUrlToEnterprise < ActiveRecord::Migration
  def up
    add_column :enterprises, :background_image_url, :string
  end
end