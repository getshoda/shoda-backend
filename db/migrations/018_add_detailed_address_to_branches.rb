class AddDetailedAddressToBranches < ActiveRecord::Migration

  def up
    remove_column :branches, :address
    add_column :branches, :address_line_1, :string
    add_column :branches, :address_line_2, :string
    add_column :branches, :postal_code, :string, default: '11000'
    add_column :branches, :city, :string, default: 'Montevideo'
    add_column :branches, :state, :string, default: 'Montevideo'
    add_column :branches, :country, :string, default: 'Uruguay'
  end

  def down
    add_column :branches, :address, :string
    remove_column :branches, :address_line_1
    remove_column :branches, :address_line_2
    remove_column :branches, :postal_code
    remove_column :branches, :city, :string
    remove_column :branches, :state, :string
    remove_column :branches, :country, :string
  end

end