class AddTagsToEnterprises < ActiveRecord::Migration

  def up
    add_column :enterprises, :tags, :string, default: [].to_yaml
  end

  def down
    remove_column :enterprises, :tags
  end
  
end
