class CreateEnterprises < ActiveRecord::Migration
  def self.up
    create_table :enterprises do |t|
      t.string  :name
      t.text    :description
    end
  end
 
  def self.down
    drop_table :enterprises
  end
end