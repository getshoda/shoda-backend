class AddPhoneToBranches < ActiveRecord::Migration

  def up
    add_column :branches, :phone, :string
  end

  def down
    remove_column :branches, :phone
  end

end