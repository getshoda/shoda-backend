class AddOpeningInfoToBranches < ActiveRecord::Migration

  def up
    add_column :branches, :open_from, :string
    add_column :branches, :open_to, :string
  end

  def down
    remove_column :branches, :open_from
    remove_column :branches, :open_to
  end

end