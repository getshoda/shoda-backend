class CreateUserBeaconInteractions < ActiveRecord::Migration
  def self.up
    create_table :user_beacon_interactions do |t|
      t.belongs_to :beacon
      t.belongs_to :user
      t.belongs_to :message
      t.datetime   :at
    end
  end
 
  def self.down
    drop_table :user_beacon_interactions
  end
end