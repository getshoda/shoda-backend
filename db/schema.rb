# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 39) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "beacons", force: true do |t|
    t.string  "uuid"
    t.integer "major"
    t.integer "minor"
    t.string  "name"
    t.string  "string_id"
    t.integer "branch_id"
  end

  create_table "beacons_messages", id: false, force: true do |t|
    t.integer "beacon_id"
    t.integer "message_id"
  end

  create_table "branches", force: true do |t|
    t.string  "name"
    t.text    "description"
    t.integer "enterprise_id"
    t.float   "latitude"
    t.float   "longitude"
    t.string  "address_line_1"
    t.string  "address_line_2"
    t.string  "postal_code",    default: "11000"
    t.string  "city",           default: "Montevideo"
    t.string  "state",          default: "Montevideo"
    t.string  "country",        default: "Uruguay"
    t.string  "phone"
    t.string  "website"
    t.string  "open_from"
    t.string  "open_to"
    t.string  "image_url"
  end

  create_table "branches_campaigns", id: false, force: true do |t|
    t.integer "branch_id"
    t.integer "campaign_id"
  end

  create_table "campaigns", force: true do |t|
    t.string   "title"
    t.datetime "from"
    t.datetime "to"
    t.integer  "enterprise_id"
    t.boolean  "offline",       default: false
  end

  add_index "campaigns", ["enterprise_id"], name: "index_campaigns_on_enterprise_id", using: :btree

  create_table "devices", force: true do |t|
    t.string  "token"
    t.string  "platform"
    t.string  "version"
    t.string  "environment"
    t.integer "user_id"
  end

  create_table "enterprises", force: true do |t|
    t.string "name"
    t.text   "description"
    t.string "open_from"
    t.string "open_to"
    t.string "tags",                 default: "--- []\n"
    t.string "website"
    t.string "logo_url"
    t.string "phone"
    t.string "background_image_url"
  end

  create_table "interests", force: true do |t|
    t.string "name"
  end

  create_table "interests_users", force: true do |t|
    t.integer "user_id"
    t.integer "interest_id"
  end

  create_table "messages", force: true do |t|
    t.string  "title"
    t.text    "body"
    t.integer "campaign_id"
    t.string  "image_url"
    t.string  "video_url"
    t.string  "terms_and_conditions"
    t.string  "rules",                default: "--- []\n"
    t.integer "ttl"
    t.string  "content",              default: "--- {}\n"
    t.boolean "welcome",              default: false
  end

  create_table "user_beacon_interactions", force: true do |t|
    t.integer  "beacon_id"
    t.integer  "user_id"
    t.integer  "message_id"
    t.datetime "at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "new_visit",  default: false
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "gender"
    t.date     "birth_date"
    t.string   "locale"
    t.string   "facebook_id"
    t.string   "facebook_token"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "password_digest"
    t.boolean  "has_password",    default: false
    t.string   "salt"
    t.string   "twitter_id"
    t.string   "twitter_token"
    t.string   "username"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

end
