require 'grape_entity'

require 'lib/utils/query'
require 'lib/utils/query'

class UserBeaconInteraction < ActiveRecord::Base
  include Utils::Query

  belongs_to :user
  belongs_to :beacon
  belongs_to :message

  set_queryable_columns :id, :beacon_id, :user_id, :message_id

  def self.last_message_for_user(user_id)
    where(['user_id = ? and message_id IS NOT NULL', user_id]).order(:at).last
  end

  def send_chain(attributes)
    attributes.inject(self) { |subject, attribute| subject.send(attribute) }
  end

  def is_a_new_visit?
    interaction = UserBeaconInteraction.joins(:beacon).where(
      'beacons.branch_id' => self.beacon.branch_id,
      user_id: user_id
    ).where.not(id: self.id).last

    interaction.nil? ||
      ((self.at - interaction.at) / 1.hour) >= CONFIGURATION.interval_between_visits
  end

  def entity
    Entity.new(self)
  end

  class Entity < Grape::Entity
    expose :id
    expose :beacon_id
    expose :user_id
    expose :message
    expose :at
  end
end
