require 'grape_entity'

require_relative 'validations/validates_existance'

require 'lib/utils/interaction_rule_set_validator'
require 'lib/utils/query'

class Message < ActiveRecord::Base
  include Utils::Query
  include Validations::ValidatesExistance

  belongs_to :campaign
  has_and_belongs_to_many :beacons

  serialize :rules, Array
  serialize :content, Hash

  validates :title, presence: { message: 'BlankTitleError - Title attribute cant be blank.' }
  validates :body,  presence: { message: 'BlankBodyError - Body attribute cant be blank.' }
  validates :campaign, existance: { message: 'InvalidCampaignIDError - Campaign must exist.' }
  validate  :valid_rule?

  set_queryable_columns :id, :title, :campaign_id

  def enabled?
    campaign && campaign.active?
  end

  def self.welcome_by_enterprise(enterprise_id)
    Message.joins(:campaign).where(
      'campaigns.enterprise_id' => enterprise_id, welcome: true
    )
  end

  def entity
    Entity.new(self)
  end

  class Entity < Grape::Entity
    expose :id
    expose :title
    expose :body
    expose :image_url
    expose :video_url
    expose :terms_and_conditions
    expose :campaign_id
    expose :rules
    expose :content
    expose :ttl
    expose :welcome
  end

  private

  def valid_rule?
    Utils::InteractionRuleSetValidator.validate!(rules) if rules
  rescue => error
    self.errors.add(:rules, error.message)
  end
end
