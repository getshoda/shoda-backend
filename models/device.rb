require 'grape_entity'
require 'logger'

require 'lib/services/ios_notifications'
require 'lib/services/android_notifications'
require 'lib/utils/query'

require_relative 'validations/validates_existance'

class Device < ActiveRecord::Base
  include Utils::Query
  include Validations::ValidatesExistance

  ANDROID = 'android'
  IOS = 'ios'

  belongs_to :user

  validates :platform, inclusion: { in: [ANDROID, IOS],
                       message: 'InvalidPlatform - %{value} is not a valid platform' }
  validates :token,    presence: { message: 'BlankTokenError - Token attribute cant be blank.' }
  validates :user_id,  presence: { message: 'BlankUserID - UserID attribute cant be blank.' }
  validates :user,     existance: { message: 'InvalidUser - The Device\'s user must exist.' }

  set_queryable_columns :id, :user_id

  def notify(message)
    unless RACK_ENV == 'test'
      Device.logger.info("*** push sent to user_id: #{user_id} with message_id: #{message.id} ***")
    end

    case platform
    when ANDROID
      Device.android_notification_server.send_notification(token, message)
    when IOS
      Device.ios_notification_server(environment).send_notification(token, message)
    end
  end

  def self.ios_notification_server(environment = nil)
    if environment == 'prod'
      @ios_notification_server_prod ||= Services::IOSNotifications.new(environment)
    else
      @ios_notification_server ||= Services::IOSNotifications.new
    end
  end

  def self.android_notification_server
    @android_notification_server ||= Services::AndroidNotifications.new
  end

  def entity
    Entity.new(self)
  end

  class Entity < Grape::Entity
    expose :id
    expose :token
    expose :platform
    expose :version
    expose :environment
    expose :user_id
  end

  def self.logger
    @logger ||= Logger.new(STDOUT)
  end
end
