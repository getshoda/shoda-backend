require 'bcrypt'
require 'securerandom'
require 'grape_entity'

require 'lib/utils/query'

class User < ActiveRecord::Base
  include Utils::Query

  has_many :devices
  has_and_belongs_to_many :interests

  after_create :create_salt

  validates :has_password, inclusion: { in: [true, false] }

  validates :gender, inclusion: {
    in: ['male', 'female'], allow_nil: true,
    message: 'InvalidGender - Gender attribute is invalid.'
  }

  set_queryable_columns :id, :email

  def password=(new_password)
    @password = BCrypt::Password.create(new_password)
    self.password_digest = @password
    self.has_password = !new_password.nil?
  end

  def authenticate(plain_password)
    has_password && BCrypt::Password.new(password_digest) == plain_password
  end

  def interest_names
    interests.collect(&:name)
  end

  def age
    return nil unless birth_date

    now = Time.now

    day_diff = now.day - birth_date.day
    month_diff = now.month - birth_date.month - (day_diff < 0 ? 1 : 0)
    now.year - birth_date.year - (month_diff < 0 ? 1 : 0)
  end

  def entity
    Entity.new(self)
  end

  class Entity < Grape::Entity
    expose :id
    expose :first_name
    expose :last_name
    expose :email
    expose :gender
    expose :birth_date
    expose :locale
    expose :facebook_id
    expose :facebook_token
    expose :created_at
    expose :updated_at
    expose :twitter_id
    expose :twitter_token
    expose :username
  end

  private

  def create_salt
    self.salt = SecureRandom.hex
  end
end
