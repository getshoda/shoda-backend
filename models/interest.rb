require 'grape_entity'

require 'lib/utils/query'

class Interest < ActiveRecord::Base
  include Utils::Query

  has_and_belongs_to_many :users

  set_queryable_columns :id, :name

  def entity
    Entity.new(self)
  end

  class Entity < Grape::Entity
    expose :id
    expose :name
  end
end
