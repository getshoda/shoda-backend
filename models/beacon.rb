require 'grape_entity'

require 'lib/utils/query'
require_relative 'validations/validates_existance'

class Beacon < ActiveRecord::Base
  include Utils::Query
  include Validations::ValidatesExistance

  belongs_to :branch
  has_and_belongs_to_many :messages

  STRING_ID_ROOT = 'com.shoda'

  validates :branch_id, :presence => { message: 'BlankBranchIDError - BranchID cant be blank.' }
  validates :uuid,      :presence => { message: 'BlankUUIDError - UUID attribute cant be blank.' }
  validates :name,      :presence => { message: 'BlankNameError - Name attribute cant be blank.' }
  validates :branch,    :existance => { message: 'InvalidBranchIDError - Branch must exist.' }

  after_create :set_string_id

  set_queryable_columns :id, :name, :branch_id

  def self.discover(major, minor, uuid)
    where(major: major, minor: minor, uuid: uuid).first
  end

  def entity
    Entity.new(self)
  end

  class Entity < Grape::Entity
    expose :id
    expose :name
    expose :uuid
    expose :major
    expose :minor
    expose :branch_id
    expose :string_id
  end

  private

  def set_string_id
    self.string_id = [STRING_ID_ROOT, self.branch.enterprise_id, self.branch.id, self.id].join('.')
    save
  end
end
