require 'grape_entity'

require 'lib/utils/query'
require_relative 'validations/validates_existance'

class Branch < ActiveRecord::Base
  include Utils::Query
  include Validations::ValidatesExistance

  belongs_to :enterprise
  has_and_belongs_to_many :campaigns

  validates :enterprise_id,
            presence: { message: 'BlankEnterpriseID - EnterpriseID cant be blank.' }
  validates :name, presence: { message: 'BlankNameError - Name attribute cant be blank.' }
  validates :enterprise,
            existance: { message: 'InvalidEnterpriseIDError - Enterprise must exist.' }

  validates :latitude,
            numericality: {
                greater_than_or_equal_to: -90.0,
                less_than_or_equal_to: 90.0,
                message: 'LatitudeValueError - Latitude must be between -90.0 and 90.0'
            },
            allow_nil: true

  validates :longitude,
            numericality: {
                greater_than_or_equal_to: -180.0,
                less_than_or_equal_to: 180.0,
                message: 'LongitudeValueError - Longitude must be between -180.0 and 180.0'
            },
            allow_nil: true

  set_queryable_columns :id, :name, :enterprise_id

  def entity
    Entity.new(self)
  end

  class Entity < Grape::Entity
    expose :id
    expose :name
    expose :description
    expose :enterprise_id
    expose :address_line_1
    expose :address_line_2
    expose :postal_code
    expose :city
    expose :state
    expose :country
    expose :latitude
    expose :longitude
    expose :phone
    expose :website
    expose :open_from
    expose :open_to
    expose :image_url
  end
end
