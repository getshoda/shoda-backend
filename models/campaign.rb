require 'grape_entity'

require 'lib/utils/query'
class Campaign < ActiveRecord::Base
  include Utils::Query

  has_and_belongs_to_many :branches
  belongs_to :enterprise
  has_many :messages

  validates :title, presence: { message: 'BlankTitleError - Title attribute cant be blank.' }
  validates :enterprise_id, presence: { message: 'BlankEnterpriseIDError - EntepriseID cant be blank' }

  def active?
    now = Time.now.to_i
    from_ts = from && from.beginning_of_day.to_i || now - 1
    to_ts   = to && to.end_of_day.to_i || now + 1

    (from_ts..to_ts).include? Time.now.to_i
  end

  set_queryable_columns :id, :title, :enterprise_id

  def entity
    Entity.new(self)
  end

  class Entity < Grape::Entity
    expose :id
    expose :title
    expose :from
    expose :to
    expose :offline
    expose :enterprise_id
    expose :branches_ids do |campaign|
      campaign.branches.collect(&:id)
    end
  end
end
