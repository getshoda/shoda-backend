module Validations
  module ValidatesExistance
    class ExistanceValidator < ActiveModel::EachValidator

      def validate_each(record, attribute_name, value)
        unless record.try(attribute_name)
          record.errors.add(attribute_name, options[:message] || default_message(attribute_name))
        end
      end

      private
      def default_message(attribute_name)
        "InvalidRelation - #{attribute_name} relation must exist."
      end
    end
  end
end