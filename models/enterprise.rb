require 'grape_entity'

require 'lib/utils/query'

class Enterprise < ActiveRecord::Base
  include Utils::Query

  has_many :branches

  serialize :tags, Array

  validates :name, presence: { message: 'BlankNameError - Name attribute cant be blank.' }

  set_queryable_columns :id, :name

  def entity
    Entity.new(self)
  end

  class Entity < Grape::Entity
    expose :id
    expose :name
    expose :description
    expose :website
    expose :logo_url
    expose :phone
    expose :tags
    expose :open_from
    expose :open_to
    expose :background_image_url
    expose :branches_count do |enterprise|
      enterprise.branches.size
    end
  end
end
