Oswald (shoda-backend)
======================

__Oswald__ is the core module of Shoda.

__Oswald__ provides the API used by the mobile applications and the admin site.


## Dependencies ##

* Ruby 2.0.0. Install Ruby 2.0.0 using [rvm](http://rvm.io/) or [rbenv](http://rbenv.org/).
* Bundler. Install with `gem install bundler`
* Redis 2.8.19 or greater. Install Redis using your OS package manager (apt on Ubuntu, yum on CentOS, Hombebrew on Mac OS X)


## Installation ##

To Install Ruby dependencies run
`bundle install`

To create the database run
`rake db:create`

To perform database migrations run
`rake db:migrate`

To drop the database run
`rake db:drop`

To perform the db taks in test environment run
`RACK_ENV=test rake ...`

To launch the server run
`rackup`

To run tests run
`rspec`


## Configuration ##

You need to create configuration files under `config`. Use the provided sample files to create them.

Some files you need:

* database.yml
* configuration.yml


## Authors ##

Created by:

* Agustín Cornú
* Juan Laube
* Ignacio Pacheco
* Renzo Caccia
* Diego Brignardello