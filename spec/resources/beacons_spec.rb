require 'spec_helper'

describe Resources::Beacons do

  include_context 'user authenticated'

  describe 'GET /beacons' do
    let!(:beacon) { FactoryGirl.create(:beacon) }
    let(:query)   { {} }

    subject(:response) { get beacons_path, query; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:beacons) }
    it { is_expected.to match_representation(:beacon, beacon, :beacons) }

    context 'with branch filter' do
      let(:branch_id) { beacon.branch_id }
      let(:query) { { branch_id__eq: branch_id } }

      it { is_expected.to match_representation(:beacon, beacon, :beacons) }

      context 'when no beacon matches the filter' do
        let(:branch_id) { -1 }

        it { is_expected.to be_an_empty_representation(:beacons) }
      end
    end
  end

  describe 'GET /beacons/:id' do
    let(:beacon)    { FactoryGirl.create(:beacon) }
    let(:beacon_id) { beacon.id }

    subject(:response) { get beacon_path(beacon_id); last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:beacon)}

    context 'when the beacon does not exist' do
      let(:beacon_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/BeaconNotFound/) }
    end
  end

  describe 'POST /beacons' do
    let(:branch) { FactoryGirl.create(:branch) }
    let(:attrs) do
      {
        uuid: '45654654',
        branch_id: branch.id,
        name: 'Kevin',
        major: rand(1_000),
        minor: rand(1_000)
      }
    end

    subject(:response) { post beacons_path, attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:beacon) }
    it { is_expected.to match_representation(:beacon, Beacon.last) }

    context 'when no uuid is given' do
      before { attrs.delete(:uuid) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when the uuid is empty' do
      before { attrs[:uuid] = '' }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end

    context 'when the branch_id is not given' do
      before { attrs.delete(:branch_id) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when the given branch is empty' do
      before { attrs[:branch_id] = -1 }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end
  end

  describe 'PUT /beacons/:id' do
    let(:beacon)    { FactoryGirl.create(:beacon) }
    let(:beacon_id) { beacon.id }
    let(:attrs)     { { uuid: '45654654', branch_id: 2 } }

    subject!(:response) { put beacon_path(beacon_id), attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:beacon) }
    it { is_expected.to match_representation(:beacon, beacon) }

    context 'when the beacons does not exist' do
      let(:beacon_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/BeaconNotFound/) }
    end
  end

  describe 'DELETE /beacons/:id' do
    let(:beacon)    { FactoryGirl.create(:beacon) }
    let(:beacon_id) { beacon.id }

    subject(:response) { delete beacon_path(beacon_id); last_response }

    it { is_expected.to be_success }

    context 'when the beacon does not exist' do
      let(:beacon_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/BeaconNotFound/) }
    end
  end
end
