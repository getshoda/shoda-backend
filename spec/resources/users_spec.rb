require 'spec_helper'

describe Resources::Users do

  include_context 'user authenticated'

  describe 'POST /users' do
    let(:attrs) do
      { first_name: 'Bruce', last_name: 'Wayne', email: 'gmail@batman.com', password: 'iambatman' }
    end

    subject(:response) { post users_path, attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:user) }
    it { is_expected.to match_representation(:user, User.last) }
  end

  describe 'GET /users' do
    subject(:response) { get users_path; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:users) }
    it { is_expected.to match_representation(:user, current_user, :users) }
  end

  describe 'GET /users/me' do

    subject(:response) { get users_me_path }

    it { is_expected.to be_success }
    
    context 'when the user is not authenticated' do
      let(:current_user) { nil }
      
      it { is_expected.to contain_error(/InvalidUser/) }
      it { is_expected.to be_unauthorized }
    end
  end

  describe 'PUT /users/:id' do
    let(:user)    { FactoryGirl.create(:user) }
    let(:user_id) { user.id }
    let(:attrs)  do
      {
        first_name: 'Brucy',
        last_name:  'Banner',
        email:      'hulk@gmail.com',
        password:   'smash'
      }
    end

    subject(:response) { put user_path(user_id), attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:user) }
    it { is_expected.to match_representation(:user, user) }

    context 'when the user does not exist' do
      let(:user_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/UserNotFound/) }
    end
  end

  describe 'PUT /users/:id/interests' do
    let(:user) { FactoryGirl.create(:user) }

    let(:clothes_interest) { FactoryGirl.create(:interest, name: 'clothes') }
    let(:games_interest)   { FactoryGirl.create(:interest, name: 'games') }

    let(:attrs) { { interests_ids: [clothes_interest.id, games_interest.id] } }

    subject(:response) { put user_interests_path(user.id), attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:user) }
    it { is_expected.to match_representation(:user, user) }

    it 'updates the user interests' do
      expect {
        put user_interests_path(user.id), attrs
        user.reload
      }.to change { user.interests.size }.by(2)
    end

    context 'when an empty array is sent' do
      let(:attrs) { { interests_ids: [] } }

      before { user.update_attribute(:interests, [clothes_interest, games_interest]) }

      it 'removes the user interests' do
        expect {
          put user_interests_path(user.id), attrs
          user.reload
        }.to change { user.interests.size }.from(2).to(0)
      end
    end

    context 'when the interest does not exist' do
      let(:attrs) { { interests_ids: [-1] } }

      it { is_expected.to be_not_found }
    end
  end

  describe 'GET /users/:id/interests' do
    let(:interest) { FactoryGirl.create(:interest) }
    let(:user) { FactoryGirl.create(:user, interests: [interest]) }

    subject(:response) { get user_interests_path(user.id); last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:interests) }
    it { is_expected.to match_representation(:interest, interest, :interests) }
  end
end
