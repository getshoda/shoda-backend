require 'spec_helper'

describe Resources::Interactions do

  include_context 'user authenticated'

  describe 'GET /users/:id/interactions' do
    let(:query)         { {} }
    let(:user)          { FactoryGirl.create(:user) }
    let(:message)       { FactoryGirl.create(:message) }
    let!(:interaction) do
      FactoryGirl.create(
        :user_beacon_interaction,
        user: current_user,
        message: message
      )
    end

    subject(:response) {
      get user_interactions_path(current_user.id), query
      last_response
    }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:interactions) }
    it { is_expected.to match_representation(:user_beacon_interaction, interaction, :interactions) }

    context 'with pagination' do
      let(:query) { { page: 2, per_page: 15 } }

      it { is_expected.to be_success }
      it { is_expected.to be_an_empty_representation(:interactions) }
    end

    context 'with filters' do
      let(:query) { { id__eq: -1 } }

      it { is_expected.to be_success }
      it { is_expected.to be_an_empty_representation(:interactions) }
    end
  end

  describe 'POST /users/:id/interactions' do
    let(:user)    { FactoryGirl.create(:user) }
    let(:message) { FactoryGirl.create(:message) }
    let(:beacon)  { FactoryGirl.create(:beacon, messages: [message]) }
    let(:now)     { Time.now }
    let(:user_id) { user.id }

    let(:attrs) do
      {
        'major' => beacon.major,
        'minor' => beacon.minor,
        'uuid'  => beacon.uuid,
        'at'    => now
      }
    end

    subject(:response) do
      post user_interactions_path(user_id), attrs
      last_response
    end

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:user_beacon_interaction) }
    it { is_expected.to match_representation(:user_beacon_interaction, UserBeaconInteraction.last) }

    context 'when the user is not found' do
      let(:user_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/UserNotFound/) }
    end

    context 'when the beacon is not found' do
      before { attrs['uuid'] = 'invalid_uuid' }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/BeaconNotFound/) }
    end
  end
end
