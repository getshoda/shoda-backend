require 'spec_helper'

describe Resources::Interests do

  include_context 'user authenticated'

  describe 'GET /interests' do
    let!(:interest) { FactoryGirl.create(:interest) }

    subject(:response) { get interests_path; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:interests) }
    it { is_expected.to match_representation(:interest, interest, :interests) }
  end
end
