require 'spec_helper'
require 'koala'
require 'jwt'

require 'spec/support/shared_contexts/twitter'
require 'spec/support/shared_contexts/facebook'

describe Auth do

  before do
    allow(JWT).to receive(:encode) { jwt_token }
  end

  let(:jwt_token) { 'Hello, I am a token' }

  shared_examples 'a successful authentication' do
    it { is_expected.to be_success }

    it 'contain token' do
      expect(JSON(subject.body)['token']).to eql jwt_token
    end
  end

  describe 'POST /login' do

    context 'using email and password' do
      let(:user) { FactoryGirl.create(:user) }
      let(:password) { 'iambatman' }

      context 'when email and password are valid' do

        subject(:response) do
          post login_path, email: user.email, password: password
          last_response
        end

        it_behaves_like 'a successful authentication'
      end

      context 'when password is not valid' do
        subject(:response) do
          post login_path, email: user.email, password: 'bar'
          last_response
        end

        it { is_expected.to be_unauthorized }
      end

      context 'when email and password are not valid' do
        subject(:response) do
          post login_path, email: 'foo', password: 'bar'
          last_response
        end

        it { is_expected.to be_unauthorized }
        it { is_expected.to contain_error('InvalidUser') }
      end
    end
  end

  describe 'POST /login/facebook' do
    include_context 'facebook successful authentication'

    let(:facebook_id) { rand(1_000).to_s }

    subject(:response) do
      post facebook_login_path, fb_user_id: facebook_id, access_token: token
      last_response
    end

    context 'when the token is valid' do
      it_behaves_like 'a successful authentication'

      context 'and the user exists' do
        let!(:user) { FactoryGirl.create(:user) }
        let(:facebook_id) { user.facebook_id }
        
        it_behaves_like 'a successful authentication'
      end
    end

    context 'when the token is invalid' do
      include_context 'facebook failed authentication'

      it { is_expected.to be_unauthorized }
      it { is_expected.to contain_error('InvalidUser') }
    end
  end

  describe 'POST login/twitter' do
    include_context 'twitter successful authentication'

    let(:token)  { rand(1_000_000).to_s }
    let(:secret) { rand(1_000_000).to_s }

    subject(:response) do
      post twitter_login_path, access_token: token, access_secret: secret
      last_response
    end

    it_behaves_like 'a successful authentication'

    context 'when the user already exists' do
      let(:user) { FactoryGirl.create(:user, twitter_id: id) }

      it_behaves_like 'a successful authentication'
    end

    context 'when the token or key are invalid' do
      include_context 'twitter failed authentication'

      it { is_expected.to be_unauthorized }
      it { is_expected.to contain_error('InvalidUser') }
    end
  end

  describe 'POST /signup' do
    let(:email) { 'foo@bar.com' }
    let(:password) { '123456' }

    subject(:response) do
      post signup_path, email: email, password: password
      last_response
    end

    it_behaves_like 'a successful authentication'

    context 'when the user already exists' do
      
      before do
        FactoryGirl.create(:user, email: email, password: password)
      end

      it { is_expected.to be_conflict }
      it { is_expected.to contain_error('EmailIsNotUnique') }
    end
  end
end