require 'spec_helper'

describe Resources::Enterprises do

  include_context 'user authenticated'

  describe 'GET /enterprises' do
    let!(:enterprise) { FactoryGirl.create(:enterprise) }
    let(:query) { {} }

    subject(:response) { get enterprises_path, query; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:enterprises) }
    it { is_expected.to match_representation(:enterprise, enterprise, :enterprises) }

    context 'with pagination' do
      let(:query) { { page: 2, per_page: 15 } }

      it { is_expected.to be_success }
      it { is_expected.to be_an_empty_representation(:enterprises) }
    end

    context 'with filters' do
      let(:query) { { name__eq: 'nope' } }

      it { is_expected.to be_success }
      it { is_expected.to be_an_empty_representation(:enterprises) }
    end
  end

  describe 'GET /enterprises/:id' do
    let(:enterprise)    { FactoryGirl.create(:enterprise) }
    let(:enterprise_id) { enterprise.id }

    subject(:response) { get enterprise_path(enterprise_id); last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:enterprise) }
    it { is_expected.to match_representation(:enterprise, enterprise) }

    context 'when the enterprise does not exist' do
      let(:enterprise_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/EnterpriseNotFound/) }
    end
  end

  describe 'POST /enterprises' do
    let(:attrs)  do
      {
        name: 'Enterprise name',
        description: 'Enterprise description',
        website: 'Enterprise website',
        logo_url: 'Enterprise logo url'
      }
    end

    subject(:response) { post enterprises_path, attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:enterprise) }
    it { is_expected.to match_representation(:enterprise, Enterprise.last) }

    context 'when no name is given' do
      before { attrs.delete(:name) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when the name is empty' do
      before { attrs[:name] = '' }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end

    context 'when a description is not given' do
      before { attrs.delete(:description) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when a website is not given' do
      before { attrs.delete(:website) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when a logo_url is not given' do
      before { attrs.delete(:logo_url) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end
  end

  describe 'PUT /enterprises/:id' do
    let(:enterprise)    { FactoryGirl.create(:enterprise) }
    let(:enterprise_id) { enterprise.id }

    let(:new_description) { 'Enterprise new_description' }
    let(:new_name)        { 'Queen Industries' }
    let(:new_website)     { 'www.queen.com' }
    let(:new_logo_url)    { 'logo' }
    let(:attrs)  do
      {
        name: new_name,
        description: new_description,
        website: new_website,
        logo_url: new_logo_url
      }
    end

    subject!(:response) { put enterprise_path(enterprise_id), attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:enterprise) }
    it { is_expected.to match_representation(:enterprise, enterprise) }

    context 'when the enterprise does not exist' do
      let(:enterprise_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/EnterpriseNotFound/) }
    end

    context 'when the name is empty' do
      let(:new_name) { '' }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end
  end

  describe 'DELETE /enterprises/:id' do
    let(:enterprise)    { FactoryGirl.create(:enterprise) }
    let(:enterprise_id) { enterprise.id }

    subject(:response) { delete enterprise_path(enterprise_id); last_response }

    it { is_expected.to be_success }

    context 'when the enterprise does not exist' do
      let(:enterprise_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/EnterpriseNotFound/) }
    end
  end
end
