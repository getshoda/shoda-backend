require 'spec_helper'

describe Resources::Messages do

  include_context 'user authenticated'

  describe 'GET /campaigns/:campaign_id/messages' do
    let!(:message) { FactoryGirl.create(:message) }

    subject(:response) { get messages_path(message.campaign_id); last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:messages) }
    it { is_expected.to match_representation(:message, message, :messages) }
  end

  describe 'GET /messages/:id' do
    let(:message)    { FactoryGirl.create(:message) }
    let(:message_id) { message.id }

    subject(:response) { get message_path(message.campaign_id, message_id); last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:message)}

    context 'when the message does not exist' do
      let(:message_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/MessageNotFound/) }
    end
  end

  describe 'POST /messages' do
    let(:campaign) { FactoryGirl.create(:campaign) }
    let(:beacon)   { FactoryGirl.create(:beacon) }
    let(:rules)    { [{ attributes: [:user, :age], operator: :greater, values: [20] }] }
    let(:attrs)    { { title: 'Title', body: 'Body', beacons_ids: [beacon.id], rules: rules, welcome: false } }

    subject(:response) { post messages_path(campaign.id), attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:message) }
    it { is_expected.to match_representation(:message, Message.last) }

    context 'when no title is given' do
      before { attrs.delete(:title) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when the title is empty' do
      before { attrs[:title] = '' }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end

    context 'when the body is not given' do
      before { attrs.delete(:body) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when the given body is empty' do
      before { attrs[:body] = '' }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end

    context 'when the rules are invalid' do
      let(:rules) { [{ attributes: [:asdf], operator: :eq, values: [1] }] }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end
  end

  describe 'PUT /messages/:id' do
    let(:message)    { FactoryGirl.create(:message) }
    let(:message_id) { message.id }
    let(:rules)      { [{ attributes: [:user, :age], operator: :greater, values: [20] }] }
    let(:attrs)      { { uuid: 'title', rules: rules } }

    subject!(:response) { put message_path(message.campaign_id, message_id), attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:message) }
    it { is_expected.to match_representation(:message, message) }

    context 'when the messages does not exist' do
      let(:message_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/MessageNotFound/) }
    end

    context 'when the rules are invalid' do
      let(:rules) { [{ attributes: [:asdf], operator: :eq, values: [1] }] }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end
  end

  describe 'DELETE /messages/:id' do
    let(:message)    { FactoryGirl.create(:message) }
    let(:message_id) { message.id }

    subject(:response) { delete message_path(message.campaign_id, message_id); last_response }

    it { is_expected.to be_success }

    context 'when the message does not exist' do
      let(:message_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/MessageNotFound/) }
    end
  end
end
