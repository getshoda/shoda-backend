require 'spec_helper'

describe Resources::Campaigns do

  include_context 'user authenticated'

  describe 'GET /campaigns' do
    let!(:campaign) { FactoryGirl.create(:campaign) }

    subject(:response) { get campaigns_path; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:campaigns) }
    it { is_expected.to match_representation(:campaign, campaign, :campaigns) }
  end

  describe 'GET /campaigns/:id' do
    let(:campaign)    { FactoryGirl.create(:campaign) }
    let(:campaign_id) { campaign.id }

    subject(:response) { get campaign_path(campaign_id); last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:campaign)}

    context 'when the campaign does not exist' do
      let(:campaign_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/CampaignNotFound/) }
    end
  end

  describe 'POST /campaigns' do
    let(:branch) { FactoryGirl.create(:branch) }

    let(:attrs) do
      {
        title:          'Some awesome title',
        branches_ids:   [branch.id],
        to:             Time.now,
        from:           Time.now,
        offline:      false,
        enterprise_id:  branch.enterprise_id
      }
    end

    subject(:response) { post campaigns_path, attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:campaign) }
    it { is_expected.to match_representation(:campaign, Campaign.last) }
  end

  describe 'DELETE /campaigns/:id' do
    let!(:campaign) { FactoryGirl.create(:campaign) }
    let(:campaign_id) { campaign.id }

    subject (:response) { delete campaign_path(campaign_id); last_response }

    it { is_expected.to be_success }

    context 'when the campaign does not exist' do
      let(:campaign_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/CampaignNotFound/) }
    end
  end
end
