require 'spec_helper'

describe Resources::Devices do

  include_context 'user authenticated'

  describe 'POST /users/:user_id/devices' do
    let(:attrs)   { { token: 'HeLLoThiSISaToKEN', platform: 'android', version: '1.0' } }
    let(:user)    { FactoryGirl.create(:user) }
    let(:user_id) { user.id }

    subject(:response) { post devices_path(user_id), attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:device) }
    it { is_expected.to match_representation(:device, Device.last) }

    context 'when no token is given' do
      before { attrs.delete(:token) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when no platform is given' do
      before { attrs.delete(:platform) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when no version is given' do
      before { attrs.delete(:version) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when the user given does not exist' do
      let(:user_id) { -1 }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end
  end
end
