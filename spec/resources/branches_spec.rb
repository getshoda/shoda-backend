require 'spec_helper'

describe Resources::Branches do

  include_context 'user authenticated'

  describe 'GET /branches' do
    let!(:branch) { FactoryGirl.create(:branch) }
    let(:query)   { {} }

    subject(:response) { get branch_path, query; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:branches) }
    it { is_expected.to match_representation(:branch, branch, :branches) }

    context 'with enterprise filter' do
      let(:enterprise_id) { branch.enterprise_id }
      let(:query) { { enterprise_id__eq: enterprise_id } }

      it { is_expected.to match_representation(:branch, branch, :branches) }

      context 'when no branch matches the filter' do
        let(:enterprise_id) { -1 }

        it { is_expected.to be_an_empty_representation(:branches) }
      end
    end
  end

  describe 'GET /branches/:id' do
    let(:branch)    { FactoryGirl.create(:branch) }
    let(:branch_id) { branch.id }

    subject(:response) { get branch_path(branch_id); last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:branch) }
    it { is_expected.to match_representation(:branch, branch) }

    context 'when the branch does not exist' do
      let(:branch_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/BranchNotFound/) }
    end
  end

  describe 'POST /branches' do
    let(:enterprise) { FactoryGirl.create(:enterprise) }

    let(:attrs) do
      {
        name: 'Branch name',
        description: 'Branch description',
        address_line_1: '8 de octubre',
        address_line_2: 'Local 101',
        postal_code: '11000',
        city: 'Montevideo',
        state: 'Montevideo',
        country: 'Uruguay',
        latitude: -56,
        longitude: -34,
        phone: '+598 1234 5678',
        website: 'http://www.foo.com/bar',
        enterprise_id: enterprise.id
      }
    end

    subject(:response) { post branches_path, attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:branch) }
    it { is_expected.to match_representation(:branch, Branch.last) }

    context 'when not name is given' do
      before { attrs.delete(:name) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when the name is empty' do
      before { attrs[:name] = '' }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end

    context 'when a description is not given' do
      before { attrs.delete(:description) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when no enterprise_id is not given' do
      before { attrs.delete(:enterprise_id) }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/MissingParameter/) }
    end

    context 'when the enterprise_id is empty' do
      before { attrs[:enterprise_id] = -1 }

      it { is_expected.to be_unprocessable_entity }
      it { is_expected.to contain_error(/ResourceInvalid/) }
    end
  end

  describe 'PUT /branches/:id' do
    let!(:branch)     { FactoryGirl.create(:branch) }
    let(:branch_id)   { branch.id }

    let(:attrs) do
      {
        name: 'Branch name',
        description: 'Branch description',
        address_line_1: '8 de octubre',
        address_line_2: 'Local 101',
        postal_code: '11000',
        city: 'Montevideo',
        state: 'Montevideo',
        country: 'Uruguay',
        latitude: -56,
        longitude: -34,
        phone: '+598 1234 5678',
        website: 'http://www.foo.com/bar'
      }
    end

    subject!(:response) { put branch_path(branch_id), attrs; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:branch) }
    it { is_expected.to match_representation(:branch, branch) }

    context 'when the branches does not exist' do
      let(:branch_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/BranchNotFound/) }
    end
  end

  describe 'DELETE /branches/:id' do
    let(:branch) { FactoryGirl.create(:branch) }
    let(:branch_id) { branch.id }

    subject(:response) { delete branch_path(branch_id); last_response }

    it { is_expected.to be_success }

    context 'when the branch does not exist' do
      let(:branch_id) { -1 }

      it { is_expected.to be_not_found }
      it { is_expected.to contain_error(/BranchNotFound/) }
    end
  end

  describe 'GET /branches/by_campaign' do
    let(:branch)      { FactoryGirl.create(:branch) }
    let(:campaign)    { FactoryGirl.create(:campaign) }
    let(:campaign_id) { campaign.id }

    before { campaign.branches << branch }

    subject(:response) { get branches_by_campaign_path, campaign_id: campaign_id; last_response }

    it { is_expected.to be_success }
    it { is_expected.to contain_element(:branches) }
    it { is_expected.to match_representation(:branch, branch, :branches) }

    context 'when the campaign does not exist' do
      let(:campaign_id) { -1 }

      it { is_expected.to be_not_found }
    end
  end
end
