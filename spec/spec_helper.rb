require 'simplecov'
SimpleCov.start

ENV['RACK_ENV'] = 'test'

require 'rack/test'
require 'rspec/its'

$: << File.join(File.dirname(__FILE__), '..')

require 'config/boot'

Dir[File.dirname(__FILE__) + '/support/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/support/*/*.rb'].each {|file| require file }
Dir[File.dirname(__FILE__) + '/factories/*.rb'].each {|file| require file }

I18n.enforce_available_locales = false

RSpec.configure do |config|
  config.include Rack::Test::Methods
  config.include Support::RoutesHelper
  
  config.around(:each) do |example|
    ::ActiveRecord::Base.transaction do
      example.run
      raise ::ActiveRecord::Rollback, 'Rollback the changes made during the example.'
    end
  end
  
  def app
    Oswald
  end
end