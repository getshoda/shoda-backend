require 'factory_girl'

FactoryGirl.define do
  factory :campaign do
    title       'Awesome campaign'
    enterprise
    from        { Time.new(2015, 3, 7) }
    to          { Time.new(2016, 3, 16) } # next saint patrick, some tests will fail :D
    offline     false
  end

  trait :expired do
    to      { Time.now - 60 * 60 * 24 }
  end

  trait :not_started do
    from    { Time.now + 60 * 60 * 24 }
  end
end