require 'factory_girl'

FactoryGirl.define do
  factory :user_beacon_interaction do
    beacon
    at        { Time.now }
  end
end
