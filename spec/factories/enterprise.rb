require 'factory_girl'

FactoryGirl.define do
  factory :enterprise do
    name            'ACME'
    description     'We love explosives'
    website         'http://getshoda.com'
    logo_url        'logo_url'
    phone           '+598 1234 5678'
    open_from       '10:00'
    open_to         '18:00'
    tags            ['a','b']
  end
end