require 'factory_girl'

FactoryGirl.define do
  factory :interest do
    name  'fish and custard'
  end
end