require 'factory_girl'

FactoryGirl.define do
  factory :device do
    token 'ThisIsAToken'
    user  

    trait :android do
      platform { Device::ANDROID }
    end

    trait :ios do
      platform { Device::IOS }
    end
  end

end