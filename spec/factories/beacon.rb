require 'factory_girl'

FactoryGirl.define do
  factory :beacon do
    name    'a beacon\'s name'
    uuid    { rand(1_000_000).to_s }
    major   { rand(1_000) }
    minor   { rand(1_000) }
    branch    
  end
end