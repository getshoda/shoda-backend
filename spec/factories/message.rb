require 'factory_girl'

FactoryGirl.define do
  factory :message do
    title                 'Bowties discount'
    body                  'Bowties are cool'
    image_url             'http://www.foobar.com/foo.jpg'
    video_url             'http://www.vimeo.com/foo.mp4'
    terms_and_conditions  'Valid from foo to bar.'
    welcome               false
    campaign 

    trait :with_rules do
      rules [{ attributes: [:user, :first_name], operator: :eq, values: ['batman'] }]
    end
  end
end