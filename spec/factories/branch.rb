require 'factory_girl'

FactoryGirl.define do
  factory :branch do
    name            'ACME suc2'
    description     'We love explosives'
    address_line_1  'somewhere part 1'
    address_line_2  'somewhere part 2'
    postal_code     '11000'
    city            'Montevideo'
    state           'Montevideo'
    country         'Montevideo'
    latitude        -56.191348
    longitude       -34.90593
    phone           '+598 1234 5678'
    website         'http://www.acme.com/suc2'
    open_from       '10:00'
    open_to         '18:00'
    enterprise      
  end

end