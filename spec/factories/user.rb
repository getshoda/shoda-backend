require 'factory_girl'

FactoryGirl.define do
  factory :user do
    first_name      'Bruce'
    last_name       'Wayne'
    gender          'male'
    facebook_id     '42'
    facebook_token  'qwerty12345'
    email           'gmail@batman.com'
    password        'iambatman'
    birth_date      { DateTime.new(1991, 7, 7) }
  end
end