require 'spec_helper'

describe Utils::Query do

  class Foo
    include Utils::Query

    set_queryable_columns :bar

    def self.where(*values); self; end

    def self.limit(value); self; end

    def self.offset(value); self; end
  end

  describe '.paginate' do
    let(:page)     { rand(1..1_00) }
    let(:per_page) { rand(1..1_00) }

    let(:paging) { { page: page, per_page: per_page } }

    it 'gets per page amount of records' do
      expect(Foo).to receive(:limit).with(per_page).and_call_original

      Foo.paginate(paging)
    end

    it 'sets the offset given the page option' do
      expected_offset = (page - 1) * per_page

      expect(Foo).to receive(:offset).with(expected_offset).and_call_original

      Foo.paginate(paging)
    end

    context 'when no paging options are given' do
      let(:paging) { {} }

      it 'sets a default limit' do
        expect(Foo).to receive(:limit).with(Utils::Query::DEFAULT_PER_PAGE).and_call_original

        Foo.paginate(paging)
      end

      it 'gets the first page' do
        expect(Foo).to receive(:offset).with(0).and_call_original

        Foo.paginate(paging)
      end
    end
  end

  describe '.filter' do
    context 'with an in filter' do
      let(:in_values)     { ['some bar'] }
      let(:filter_params) { { bar__in: in_values } }

      it 'build an in query' do
        expect(Foo).to receive(:where).with('bar in (?)', in_values).and_call_original

        Foo.filter(filter_params)
      end
    end

    context 'with a like filter' do
      let(:like_value)    { 'bar' }
      let(:filter_params) { { bar__like: like_value } }

      it 'build a like query' do
        expect(Foo).to receive(:where).with('bar ilike ?', "%#{like_value}%").and_call_original

        Foo.filter(filter_params)
      end
    end

    context 'with a eq filter' do
      let(:eq_value)      { 'bar' }
      let(:filter_params) { { bar__eq: eq_value } }

      it 'build a like query' do
        expect(Foo).to receive(:where).with('bar = ?', eq_value).and_call_original

        Foo.filter(filter_params)
      end
    end

    context 'when the attribute is invalid' do
      let(:eq_value)      { 'bar' }
      let(:filter_params) { { invalid__eq: eq_value } }

      it 'raises InvalidAttribute' do
        expect {
          Foo.filter(filter_params)
        }.to raise_error(InvalidAttribute)
      end
    end

    context 'when the operation is invalid' do
      let(:value)         { 'bar' }
      let(:filter_params) { { bar__invalid: value } }

      it 'raises InvalidAttributeError' do
        expect {
          Foo.filter(filter_params)
        }.to raise_error(InvalidOperation)
      end
    end
  end
end
