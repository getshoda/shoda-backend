require 'spec_helper'

require 'lib/utils/interaction_rule'

describe Utils::InteractionRule do
  let(:user) { FactoryGirl.create(:user) }
  let(:interaction) { FactoryGirl.create(:user_beacon_interaction, user: user) }

  subject(:rule) { Utils::InteractionRule.new(interaction, attributes, operator, values) }

  describe '#eval' do
    subject { rule.eval }

    context 'with eq operator' do
      let(:operator)   { :eq }
      let(:attributes) { [:user, :first_name] }

      context 'when the subject and the expected value are equal' do
        let(:values) { [user.first_name] }

        it { is_expected.to be }
      end

      context 'when the subject and the expected value are not equal' do
        let(:values) { [user.first_name + 'nope'] }

        it { is_expected.to_not be }
      end
    end

    context 'with includes operator' do
      let(:operator) { :includes }

      let(:batman_interest) { FactoryGirl.create(:interest, name: 'batman') }
      let(:dr_who_interest) { FactoryGirl.create(:interest, name: 'dr_who') }

      let(:user) do
        FactoryGirl.create(:user, interests: [batman_interest, dr_who_interest])
      end

      let(:attributes) { [:user, :interests] }

      context 'when the expected values are included in the subject' do
        let(:values) { [batman_interest] }

        it { is_expected.to be }
      end

      context 'when the expected values are not included in the subject' do
        let(:values) { [FactoryGirl.create(:interest, name: 'superman')] }

        it { is_expected.to_not be }
      end
    end

    context 'with between operator' do
      let(:operator) { :between }

      let(:attributes) { [:at] }

      context 'when the subject is between the values' do
        let(:values) { [interaction.at - 1000, interaction.at + 1000] }

        it { is_expected.to be }
      end

      context 'when the subject is not between the values' do
        let(:values) { [interaction.at - 2000, interaction.at - 1000] }

        it { is_expected.to_not be }
      end
    end

    context 'with greater operator' do
      let(:operator) { :greater }
      let(:attributes) { [:user, :age] }
      let(:age) { 18 }

      before { allow(user).to receive(:age) { age } }

      context 'when the subject is greater than the value' do
        let(:values) { [15] }

        it { is_expected.to be }
      end

      context 'when the subject is not greater than the value' do
        let(:values) { [19] }

        it { is_expected.to_not be }
      end

      context 'when the subject is nil' do
        let(:age) { nil }
        let(:values) { [19] }

        it { is_expected.to be }
      end
    end
  end
end
