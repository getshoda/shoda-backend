require 'spec_helper'

require 'lib/utils/interaction_rule_set_validator'

describe Utils::InteractionRuleSetValidator do

  describe '.validate!' do
    let(:rule)     {{ attributes: attributes, operator: operator, values: values }}
    let(:rule_set) { [rule, rule] }

    context 'when the ruleset is empty' do
      let(:rule_set) { [] }

      it 'returns nil' do
        expect(described_class.validate!(rule_set)).to be_nil
      end
    end

    context 'when empty attributes are given' do
      let(:attributes) { [] }
      let(:operator)   { :eq }
      let(:values)     { [1, 2] }

      it 'raises EmptyRuleAttributesError' do
        expect {
          described_class.validate!(rule_set)
        }.to raise_error(EmptyRuleAttributesError)
      end
    end

    context 'when an invalid attribute' do
      let(:attributes) { [:invalid] }
      let(:operator)   { :eq }
      let(:values)     { [1, 2] }

      it 'raises InvalidRuleAttribute' do
        expect {
          described_class.validate!(rule_set)
        }.to raise_error(InvalidRuleAttribute)
      end
    end

    context 'when an invalid nested attribute is given' do
      let(:attributes) { [:user, :invalid] }
      let(:operator)   { :eq }
      let(:values)     { [1, 2] }

      it 'raises InvalidRuleAttribute' do
        expect {
          described_class.validate!(rule_set)
        }.to raise_error(InvalidRuleAttribute)
      end
    end

    context 'when the operator is invalid' do
      let(:attributes) { [:user, :age] }
      let(:operator)   { :nope }
      let(:values)     { [1] }      

      it 'raises InvalidRuleOperator' do
        expect {
          described_class.validate!(rule_set)
        }.to raise_error(InvalidRuleOperator)
      end
    end

    context 'when the operator is invalid' do
      let(:attributes) { [:user, :age] }
      let(:operator)   { nil }
      let(:values)     { [1] }      

      it 'raises InvalidRuleOperator' do
        expect {
          described_class.validate!(rule_set)
        }.to raise_error(BlankRuleOperator)
      end
    end

    context 'with valid attributes, operator and values' do
      let(:attributes) { [:user, :age] }
      let(:operator)   { :eq }
      let(:values)     { [1, 2] }

      it 'returns nil' do
        expect(described_class.validate!(rule_set)).to be_nil
      end
    end
  end
end