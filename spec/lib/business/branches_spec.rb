require 'spec_helper'
require 'lib/business/branches'

describe Business::Branches do

  subject(:business) { Business::Branches.new }

  describe '#find' do
    let(:branch) { FactoryGirl.create(:branch) }

    it 'returns the branch for the given id' do
      expect(business.find(branch.id)).to eql branch
    end

    context 'when the branch does not exist' do
      it 'raises BranchNotFound error' do
        expect {
          business.find(-1)
        }.to raise_error(BranchNotFound)
      end
    end
  end

  describe '#get' do
    let!(:branch) { FactoryGirl.create(:branch) }

    it 'returns all the branches' do
      expect(business.get).to include(branch)
    end

    context 'when a enterprise_id is given' do
      it 'returns the matching branches' do
        expect(business.get(enterprise_id__eq: branch.enterprise_id)).to include(branch)
      end

      context 'when no branch matches the filter' do
        let(:enterprise_id) { -1 }

        it 'returns an empty set' do
          expect(business.get(enterprise_id__eq: -1)).to be_empty
        end
      end
    end
  end

  describe '#create' do
    let(:name)           { 'Branch 2' }
    let(:description)    { 'Outlet' }
    let(:address_line_1) { '8 de italy 2134' }
    let(:address_line_2) { 'Local 101' }
    let(:postal_code)    { '11000' }
    let(:city)           { 'Montevideo' }
    let(:state)          { 'Montevideo' }
    let(:country)        { 'Uruguay' }
    let(:latitude)       { -56.191348 }
    let(:longitude)      { -34.90593 }
    let(:phone)          { '+598 1234 5678' }
    let(:website)        { 'http://www.acme.com/branch2' }
    let(:open_from)      { '10:00' }
    let(:open_to)        { '18:00' }
    let(:enterprise_id)  { FactoryGirl.create(:enterprise).id }

    let(:branch_attrs) do
      { name: name,
        description: description,
        address_line_1: address_line_1,
        address_line_2: address_line_2,
        postal_code: postal_code,
        city: city,
        state: state,
        country: country,
        latitude: latitude,
        longitude: longitude,
        phone: phone,
        website: website,
        enterprise_id: enterprise_id }
    end

    it 'creates a branches' do
      expect {
        business.create(branch_attrs)
      }.to change { Branch.count }.by(1)
    end

    describe 'the created branch' do
      subject { business.create(branch_attrs) }

      its(:name)            { is_expected.to eql name }
      its(:description)     { is_expected.to eql description }
      its(:address_line_1)  { is_expected.to eql address_line_1 }
      its(:address_line_2)  { is_expected.to eql address_line_2 }
      its(:postal_code)     { is_expected.to eql postal_code }
      its(:city)            { is_expected.to eql city }
      its(:state)           { is_expected.to eql state }
      its(:country)         { is_expected.to eql country }
      its(:latitude)        { is_expected.to eql latitude }
      its(:longitude)       { is_expected.to eql longitude }
      its(:phone)           { is_expected.to eql phone }
      its(:website)         { is_expected.to eql website }
      its(:enterprise_id)   { is_expected.to eql enterprise_id }
    end

    context 'when the name is empty' do
      let(:name) { '' }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(branch_attrs)
        }.to raise_error(ResourceInvalid)
      end
    end

    context 'when the enterprise given does not exist' do
      let(:enterprise_id) { -1 }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(branch_attrs)
        }.to raise_error(ResourceInvalid)
      end
    end
  end

  describe '#update' do
    let(:branch)    { FactoryGirl.create(:branch) }
    let(:new_attrs) { { name: 'Badass name' } }

    it 'updates the given attributes' do
      expect {
        business.update(branch.id, new_attrs)
        branch.reload
      }.to change { branch.name }.to(new_attrs[:name])
    end

    context 'when the branch is not found' do
      it 'raises BranchNotFound' do
        expect {
          business.update(-1, new_attrs)
        }.to raise_error(BranchNotFound)
      end
    end
  end

  describe '#delete' do
    let!(:branch) { FactoryGirl.create(:branch) }

    it 'deletes the branch' do
      expect {
        business.delete(branch.id)
      }.to change { Branch.count }.by(-1)
    end

    context 'when the branch does not exist' do
      it 'raises BranchNotFound error' do
        expect {
          business.delete(-1)
        }.to raise_error(BranchNotFound)
      end
    end
  end

  describe '#by_campaign' do
    let(:campaign)        { FactoryGirl.create(:campaign) }
    let(:branch)          { FactoryGirl.create(:branch) }
    let(:other_campaign)  { FactoryGirl.create(:campaign) }

    before { campaign.branches << branch }

    it 'returns branches with a given campaign' do
      expect(business.by_campaign(campaign.id)).to include(branch)
    end

    it 'does not return branches without the campaign' do
      expect(business.by_campaign(other_campaign.id)).to_not include(branch)
    end

    context 'when the campaign does not exist' do
      it 'raises CampaignNotFound error' do
        expect {
          business.by_campaign(-1)
        }.to raise_error(CampaignNotFound)
      end
    end
  end
end