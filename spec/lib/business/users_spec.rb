require 'spec_helper'
require 'spec/support/shared_contexts/twitter'
require 'spec/support/shared_contexts/facebook'

require 'koala'

describe Business::Users do
  subject(:business) { Business::Users.new }

  describe '#create' do
    let(:attrs) do
      { 
        first_name: 'Bruce',
        last_name: 'Wayne',
        email: 'bruce@wayne.com',
        gender: 'male',
        email: 'gmail@batmam.com',
        password: 'iambatman' 
      }
    end

    it 'creates a new user' do
      expect {
        business.create(attrs)
      }.to change { User.count }.by(1)
    end

    describe 'the created user' do
      subject { business.create(attrs) }

      its(:first_name)  { is_expected.to eql attrs[:first_name] }
      its(:last_name)   { is_expected.to eql attrs[:last_name] }
      its(:email)       { is_expected.to eql attrs[:email] }
      its(:gender)      { is_expected.to eql attrs[:gender] }
    end
  end

  describe '#login_with_facebook' do
    include_context 'facebook successful authentication'

    let(:facebook_id) { rand(1_000).to_s }

    context 'when the token is valid' do
      context 'when the user does not exist' do 
        it 'authenticates and creates a new user' do
          expect {
            business.login_with_facebook(facebook_id, token)
          }.to change { User.count }.by(1)
        end

        it 'returns the new user' do
          expect(business.login_with_facebook(facebook_id, token)).to eql User.last
        end
      end

      context 'when the user already exists' do
        let(:user) { FactoryGirl.create(:user) }

        it 'updates its facebook token' do
          expect {
            business.login_with_facebook(user.facebook_id, token)
            user.reload
          }.to change { user.facebook_token }.to(token)
        end

        it 'returns the user' do
          expect(business.login_with_facebook(user.facebook_id, token)).to eql user
        end
      end
    end

    context 'when the token is invalid' do
      include_context 'facebook failed authentication'

      it 'raises InvalidUser' do
        expect {
          business.login_with_facebook(facebook_id, token)
        }.to raise_error(InvalidUser)
      end

      context 'when the user already exists' do
        let(:user) { FactoryGirl.create(:user) }

        it 'also raises InvalidUser' do
          expect {
            business.login_with_facebook(user.facebook_id, token)
          }.to raise_error(InvalidUser)
        end
      end
    end
  end

  describe '#login_with_twitter' do
    let(:token)  { rand(1_000_000).to_s }
    let(:secret) { rand(1_000_000).to_s }

    include_context 'twitter successful authentication'

    it 'creates a new user' do
      expect {
        business.login_with_twitter(token, secret)
      }.to change { User.count }.by(1)
    end

    describe 'the created user' do
      subject { business.login_with_twitter(token, secret) }

      its(:username)      { is_expected.to eql screen_name }
      its(:twitter_id)    { is_expected.to eql id }
      its(:twitter_token) { is_expected.to eql token }
    end

    context 'when the user already exists' do
      let!(:user) { FactoryGirl.create(:user, twitter_id: id) }

      it 'does not create a new user' do
        expect {
          business.login_with_twitter(token, secret)
        }.to_not change { User.count }
      end

      it 'updates the twitter token' do
        expect {
          business.login_with_twitter(token, secret)
          user.reload
        }.to change { user.twitter_token }.to(token)
      end
    end

    context 'when the twitter authentication fails' do
      include_context 'twitter failed authentication'

      it 'raises InvalidUser error' do
        expect {
          business.login_with_twitter(token, secret)
        }.to raise_error(InvalidUser)
      end
    end
  end
end