require 'spec_helper'

describe Business::Devices do 

  subject(:business) { Business::Devices.new }

  describe '#create' do
    let(:user)  { FactoryGirl.create(:user) }
    let(:attrs) { { token: 'HeLLoThiSISaToKEN', platform: 'android', version: '1.0' } }

    it 'creates a new device' do
      expect {
        business.create(user.id, attrs)
      }.to change { Device.count }.by(1)
    end

    context 'when the platform given is invalid' do
      before { attrs[:platform] = 'blackberry' }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(user.id, attrs)
        }.to raise_error(ResourceInvalid)
      end
    end

    context 'when no token is given' do
      before { attrs.delete(:token) }
      
      it 'raises ResourceInvalid error' do
        expect {
          business.create(user.id, attrs)
        }.to raise_error(ResourceInvalid)
      end
    end

    context 'when no user_id is given' do
      it 'raises ResourceInvalid error' do
        expect {
          business.create(nil, attrs)
        }.to raise_error(ResourceInvalid)
      end
    end

    context 'when the user given does not exist' do
      it 'raises ResourceInvalid error' do
        expect {
          business.create(-1, attrs)
        }.to raise_error(ResourceInvalid)
      end
    end

    describe 'the created device' do
      subject { business.create(user.id, attrs) }

      its(:user_id)   { is_expected.to eql user.id }
      its(:token)     { is_expected.to eql attrs[:token] }
      its(:platform)  { is_expected.to eql attrs[:platform] }
      its(:version)   { is_expected.to eql attrs[:version] }
    end

    context 'when the device already exists' do
      before { business.create(user.id, attrs) }

      it 'does not create the device' do
        expect {
          business.create(user.id, attrs)
        }.to_not change { Device.count }
      end
    end
  end
end