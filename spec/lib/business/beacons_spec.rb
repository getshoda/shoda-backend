require 'spec_helper'

describe Business::Beacons do

  subject(:business) { Business::Beacons.new }

  describe '#find' do
    let(:beacon) { FactoryGirl.create(:beacon) }

    it 'returns the beacon for the given id' do
      expect(business.find(beacon.id)).to eql beacon
    end

    context 'when the beacon does not exist' do
      it 'raises BeaconNotFound error' do
        expect {
          business.find(-1)
        }.to raise_error(BeaconNotFound)
      end
    end
  end

  describe '#get' do
    let(:beacon) { FactoryGirl.create(:beacon) }

    it 'returns all the beacons' do
      expect(business.get).to include(beacon)
    end

    context 'when a branch_id is given' do
      it 'returns the matching beacons' do
        expect(business.get({ branch_id__eq: beacon.branch_id })).to include(beacon)
      end

      context 'when no beacon matches the filter' do
        let(:branch_id) { -1 }

        it 'returns an empty set' do
          expect(business.get({ branch_id__eq: -1 })).to be_empty
        end
      end
    end
  end

  describe '#create' do
    let(:uuid)         { '25435425432' }
    let(:branch)       { FactoryGirl.create(:branch) }
    let(:name)         { 'Kevin' }
    let(:beacon_attrs) { { uuid: uuid, branch_id: branch.id, name: name } }

    it 'creates a beacons' do
      expect {
        business.create(beacon_attrs)
      }.to change { Beacon.count }.by(1)
    end

    describe 'the created beacon' do
      subject { business.create(beacon_attrs) }

      its(:name)      { is_expected.to eql name }
      its(:uuid)      { is_expected.to eql uuid }
      its(:branch_id) { is_expected.to eql branch.id }
    end

    context 'when the uuid is empty' do
      let(:uuid) { '' }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(beacon_attrs)
        }.to raise_error(ResourceInvalid)
      end
    end
  end

  describe '#update' do
    let(:beacon)    { FactoryGirl.create(:beacon) }
    let(:new_attrs) { { name: 'KevinBacon' } }

    it 'updates the given attributes' do
      expect {
        business.update(beacon.id, new_attrs)
        beacon.reload
      }.to change { beacon.name }.to(new_attrs[:name])
    end

    context 'when the beacon does not exist' do
      it 'raises BeaconNotFound' do
        expect {
          business.update(-1, new_attrs)
        }.to raise_error(BeaconNotFound)
      end
    end
  end

  describe '#delete' do
    let!(:beacon) { FactoryGirl.create(:beacon) }

    it 'deletes the beacon' do
      expect {
        business.delete(beacon.id)
      }.to change { Beacon.count }.by(-1)
    end

    context 'when the beacon does not exist' do
      it 'raises BeaconNotFound error' do
        expect {
          business.delete(-1)
        }.to raise_error(BeaconNotFound)
      end
    end
  end

  describe '#discover' do
    let!(:beacon) { FactoryGirl.create(:beacon) }

    it 'returns the first beacon matching the data' do
      expect(business.discover(beacon.major, beacon.minor, beacon.uuid)).to eql beacon
    end

    context 'when no beacon found' do
      it 'raises BeaconNotFound' do
        expect {
          business.discover(-1, -1, 'invalid')
        }.to raise_error(BeaconNotFound)
      end
    end
  end
end