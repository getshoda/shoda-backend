require 'spec_helper'

require 'lib/business/messages'

describe Business::UserBeaconInteractions do

  subject(:business) { Business::UserBeaconInteractions.new }

  describe '#get' do
    let(:query) { {} }
    let(:user) { FactoryGirl.create(:user)}
    let(:message) { FactoryGirl.create(:message) }
    let!(:interaction) do
      FactoryGirl.create(
        :user_beacon_interaction,
        user: user,
        message: message
      )
    end

    it 'returns all the messages for the given user' do
      expect(business.get(user.id, query)).to include interaction
    end
  end

  describe '#create' do
    let(:user)     { FactoryGirl.create(:user) }
    let(:campaign) { FactoryGirl.create(:campaign) }
    let(:message)  { FactoryGirl.create(:message, campaign: campaign) }
    let(:beacon)   { FactoryGirl.create(:beacon, messages: [message]) }
    let(:now)      { Time.now }

    let(:attrs) do
      {
        'major' => beacon.major,
        'minor' => beacon.minor,
        'uuid'  => beacon.uuid,
        'at'    => now
      }
    end

    it 'creates a new user beacon interaction' do
      expect {
        business.create(user.id, attrs)
      }.to change { UserBeaconInteraction.count }.by(1)
    end

    context 'with an android device' do
      let(:device) { FactoryGirl.create(:device, :android, user: user) }

      it 'sends an android push notification' do
        expect(
          Device.android_notification_server
        ).to receive(:send_notification).with(device.token, message)

        business.create(user.id, attrs)
      end
    end

    context 'with an ios device' do
      let(:device) { FactoryGirl.create(:device, :ios, user: user) }

      it 'sends an ios push notification' do
        expect(
          Device.ios_notification_server
        ).to receive(:send_notification).with(device.token, message)

        business.create(user.id, attrs)
      end
    end

    describe 'the created user beacon interaction' do
      subject { business.create(user.id, attrs) }

      its(:beacon_id)  { is_expected.to eql beacon.id }
      its(:user_id)    { is_expected.to eql user.id }
      its(:message_id) { is_expected.to eql message.id }
      its(:at)         { is_expected.to eql now }

      context 'when the user already received a notification in the allowed lapse' do
        before { business.create(user.id, attrs.dup) }

        its(:message_id) { is_expected.to be_nil }
      end

      context 'without a rule' do
        before { message.update_attribute(:rules, nil) }

        its(:message_id) { is_expected.to eq message.id }
      end

      context 'with rules' do
        let(:message) { FactoryGirl.create(:message, :with_rules) }

        context 'when the rule does not aplly' do
          its(:message_id) { is_expected.to be_nil }
        end

        context 'when a rule applies' do
          let(:user) { FactoryGirl.create(:user, first_name: message.rules.first[:values].first) }
          # ^ Not the more elegant way to do this, but...

          its(:message_id) { is_expected.to eql message.id }
        end

        context 'when a rule does not apply' do
          let(:user) do
            FactoryGirl.create(:user, first_name: message.rules.first[:values].first + '!')
          end

          its(:message_id) { is_expected.to be_nil }
        end

        context 'with a multiple rule' do
          let(:age)    { user.age - 1 }
          let(:gender) { user.gender }

          let(:rules) do
            [
              {
                'attributes' => ['user', 'age'],
                'operator' => 'greater',
                'values' => [age]
              },
              {
                'attributes' => ['user', 'gender'],
                'operator' => 'eq',
                'values' => [gender]
              }
            ]
          end

          before { message.update_attribute(:rules, rules) }

          context 'when the rule applies' do
            its(:message_id) { is_expected.to eql message.id }
          end

          context 'when the rule does not apply' do
            let(:age)    { user.age + 1 }
            let(:gender) { user.gender + '_invalid' }

            its(:message_id) { is_expected.to be_nil }
          end
        end
      end

      context 'when the messages for the beacons has no active campaign' do
        let(:campaign) { FactoryGirl.create(:campaign, :expired) }

        its(:message_id) { is_expected.to be_nil }
      end
    end

    context 'when the beacon is not found' do
      before { attrs['uuid'] = 'nonexistant' }

      it 'raises BeaconNotFoundError' do
        expect {
          business.create(user.id, attrs)
        }.to raise_error(BeaconNotFound)
      end
    end

    context 'when the user is not found' do
      it 'raises UserNotFoundError' do
        expect {
          business.create(-1, attrs)
        }.to raise_error(UserNotFound)
      end
    end
  end
end
