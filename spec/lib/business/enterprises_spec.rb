require 'spec_helper'

describe Business::Enterprises do

  subject(:business) { Business::Enterprises.new }

  describe '#find' do
    let(:enterprise) { FactoryGirl.create(:enterprise) }

    it 'returns the enterprise for the given id' do
      expect(business.find(enterprise.id)).to eql enterprise
    end

    context 'when the enterprise does not exist' do
      it 'raises EnterpriseNotFoudn error' do
        expect {
          business.find(-1)
        }.to raise_error(EnterpriseNotFound)
      end
    end
  end

  describe '#get' do
    let(:query)      { {} }
    let(:enterprise) { FactoryGirl.create(:enterprise) }

    it 'returns all the enterprises' do
      expect(business.get(query)).to include(enterprise)
    end
  end

  describe '#create' do
    let(:name)             { 'ACME' }
    let(:description)      { 'Pay one take none' }
    let(:website)          { 'http://getshoda.com' }
    let(:logo_url)         { 'logo_url' }
    let(:open_from)        { '10:00' }
    let(:open_to)          { '22:00' }
    let(:tags)             { ['a','b','c'] }

    let(:enterprise_attrs) do {
      name: name,
      description: description,
      website: website,
      logo_url: logo_url,
      open_from: open_from,
      open_to: open_to,
      tags: tags
    }

    end

    it 'creates a enterprise' do
      expect {
        business.create(enterprise_attrs)
      }.to change { Enterprise.count }.by(1)
    end

    describe 'the created enterprise' do
      subject { business.create(enterprise_attrs) }

      its(:name)        { is_expected.to eql name }
      its(:description) { is_expected.to eql description }
      its(:website)     { is_expected.to eql website }
      its(:logo_url)    { is_expected.to eql logo_url }
      its(:open_from)   { is_expected.to eql open_from }
      its(:open_to)     { is_expected.to eql open_to }
      its(:tags)        { is_expected.to eql tags }
    end

    context 'when the name is empty' do
      let(:name) { '' }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(enterprise_attrs)
        }.to raise_error(ResourceInvalid)
      end
    end
  end

  describe '#update' do
    let(:enterprise) { FactoryGirl.create(:enterprise) }
    let(:name)       { 'WayneCorp' }
    let(:attrs)      { { name: name } }

    it 'updates a enterprise' do
      expect {
        business.update(enterprise.id, attrs)
        enterprise.reload
      }.to change { enterprise.name }.to(name)
    end

    context 'when the enterprise does not exist' do
      it 'raises EnterpriseNotFound' do
        expect {
          business.update(-1, attrs)
        }.to raise_error(EnterpriseNotFound)
      end
    end

    context 'when the name is empty' do
      let(:name) { '' }

      it 'raises ResourceInvalid error' do
        expect {
          business.update(enterprise.id, attrs)
        }.to raise_error(ResourceInvalid)
      end
    end
  end

  describe '#delete' do
    let!(:enterprise) { FactoryGirl.create(:enterprise) }

    it 'deletes the enterprise' do
      expect {
        business.delete(enterprise.id)
      }.to change { Enterprise.count }.by(-1)
    end

    context 'when the enterprise does not exist' do
      it 'raises EnterpriseNotFound error' do
        expect {
          business.delete(-1)
        }.to raise_error(EnterpriseNotFound)
      end
    end
  end
end