require 'spec_helper'

require 'mock_redis'

require 'lib/business/sessions'

describe Business::Sessions do
  let(:redis_instance) { MockRedis.new } 

  before do
    allow(Redis).to receive(:new) { redis_instance }
  end

  subject(:business) { Business::Sessions.new }

  let(:user) { FactoryGirl.create(:user) }

  describe '#get_session' do
    it 'returns a JSON Web Token for the given user and stores session in redis' do
      expect(business.get_session(user)).to be
    end

    context 'when the user is nil' do
      it 'returns nil' do
        expect(business.get_session(nil)).to_not be
      end
    end
  end

  describe '#find_user_with_token' do
    let(:token) { business.get_session(user) }

    it 'returns the user for the given token' do
      expect(business.find_user_with_session(token)).to eql user
    end

    context 'when the token is not valid' do
      let(:token) { 'An invalid JSON Web Token' }
      it 'returns nil' do
        expect {
          business.find_user_with_session(token)
        }.to raise_error(UserNotFound)
      end
    end
  end

  describe '#destroy_session' do
    let(:token) { business.get_session(user) }

    before do
      business.destroy_session(token)
    end

    it 'deletes the token from the redis cache' do
      expect {
        business.find_user_with_session(token)
      }.to raise_error(UserNotFound)
    end
  end
end