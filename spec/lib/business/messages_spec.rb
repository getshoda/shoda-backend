require 'spec_helper'

require 'lib/business/messages'

describe Business::Messages do

  subject(:business) { Business::Messages.new }

  describe '#find' do
    let(:message) { FactoryGirl.create(:message) }

    it 'returns the message for the given id' do
      expect(business.find(message.id)).to eql message
    end

    context 'when the message does not exist' do
      it 'raises MessageNotFound error' do
        expect {
          business.find(-1)
        }.to raise_error(MessageNotFound)
      end
    end
  end

  describe '#get' do
    let(:message) { FactoryGirl.create(:message) }

    it 'returns all the messages of a campaign' do
      expect(business.get(message.campaign_id)).to include(message)
    end

    it 'does not return the branches from another campaign' do
      expect(business.get(-1)).to_not include(message)
    end
  end

  describe '#create' do
    let(:title)       { 'I am a title' }
    let(:body)        { 'And I am a body' }
    let(:campaign)    { FactoryGirl.create(:campaign) }
    let(:beacon)      { FactoryGirl.create(:beacon) }
    let(:campaign_id) { campaign.id }
    let(:beacons_ids) { [beacon.id] }
    let(:rules)       { [{ attributes: [:user, :first_name], operator: :eq, values: [1] }] }
    let(:welcome)     { false }

    let(:message_attrs) do
      {
        'title'       => title,
        'body'        => body,
        'campaign_id' => campaign_id,
        'beacons_ids' => beacons_ids,
        'rules'       => rules,
        'welcome'     => welcome
      }
    end

    it 'creates a messages' do
      expect {
        business.create(message_attrs)
      }.to change { Message.count }.by(1)
    end

    describe 'the created message' do
      subject { business.create(message_attrs) }

      its(:title)       { is_expected.to eql title }
      its(:body)        { is_expected.to eql body }
      its(:campaign_id) { is_expected.to eql campaign_id }
      its(:beacons)     { is_expected.to include beacon }
      its(:rules)       { is_expected.to eql rules }
      its(:welcome)     { is_expected.to eql welcome }
    end

    it 'adds the message to the campaign messages' do
      expect {
        business.create(message_attrs)
      }.to change { campaign.messages.count }.by(1)
    end

    context 'when the title is empty' do
      let(:title) { '' }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(message_attrs)
        }.to raise_error(ResourceInvalid)
      end
    end

    context 'when the body is empty' do
      let(:body) { '' }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(message_attrs)
        }.to raise_error(ResourceInvalid)
      end
    end

    context 'when the campaign does not exist' do
      let(:campaign_id) { -1 }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(message_attrs)
        }.to raise_error(ResourceInvalid)
      end
    end
  end

  describe '#update' do
    let(:message)    { FactoryGirl.create(:message) }
    let(:new_attrs)  { { title: 'A cooler title', body: 'A cooler body', welcome: true } }

    it 'updates the title' do
      expect {
        business.update(message.id, new_attrs)
        message.reload
      }.to change { message.title }.to(new_attrs[:title])
    end

    it 'updates the body' do
      expect {
        business.update(message.id, new_attrs)
        message.reload
      }.to change { message.body }.to(new_attrs[:body])
    end

    it 'updates the welcome flag' do
      expect {
        business.update(message.id, new_attrs)
        message.reload
      }.to change { message.welcome? }.to(new_attrs[:welcome])
    end

    context 'when the message does not exist' do
      it 'raises MessageNotFound' do
        expect {
          business.update(-1, new_attrs)
        }.to raise_error(MessageNotFound)
      end
    end
  end

  describe '#delete' do
    let!(:message) { FactoryGirl.create(:message) }

    it 'deletes the message' do
      expect {
        business.delete(message.id)
      }.to change { Message.count }.by(-1)
    end

    context 'when the message does not exist' do
      it 'raises MessageNotFound error' do
        expect {
          business.delete(-1)
        }.to raise_error(MessageNotFound)
      end
    end
  end

  describe '#get_by_context' do
    let(:interaction) { FactoryGirl.create(:user_beacon_interaction) }

    context 'when it is a new visit' do

      it 'updates the interaction' do
        expect {
          business.get_by_context(interaction)
        }.to change { interaction.new_visit? }.from(false).to(true)
      end
    end
  end
end
