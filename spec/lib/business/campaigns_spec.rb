require 'spec_helper'
require 'lib/business/campaigns'

describe Business::Campaigns do
  subject(:business) { Business::Campaigns.new }

  describe '#create' do
    let(:branch) { FactoryGirl.create(:branch) }
    let(:enterprise) { FactoryGirl.create(:enterprise) }
    let(:day_in_seconds) { 24 * 60 * 60 }

    let(:attrs) do
      {
        'title' => 'Discounts in bowties',
        'branches_ids' => [branch.id],
        'enterprise_id' => enterprise.id,
        'from' => Time.now - day_in_seconds,
        'to' => Time.now + day_in_seconds
       }
     end

    it 'creates a new campaign' do
      expect {
        business.create(attrs)
      }.to change { Campaign.count }.by(1)
    end

    context 'when the title is not given' do
      before { attrs.delete('title') }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(attrs)
        }.to raise_error(ResourceInvalid)
      end
    end

    context 'when the enterprise is not given' do
      before { attrs.delete('enterprise_id') }

      it 'raises ResourceInvalid error' do
        expect {
          business.create(attrs)
        }.to raise_error(ResourceInvalid)
      end
    end
  end

  describe '#get' do
    let(:campaign) { FactoryGirl.create(:campaign) }

    it 'returns all the campaigns' do
      expect(business.get).to include(campaign)
    end
  end

  describe '#find' do
    let(:campaign) { FactoryGirl.create(:campaign) }

    it 'returns the campaign' do
      expect(business.find(campaign.id)).to eql campaign
    end

    context 'when the campaign does not exist' do
      it 'raise CampaignNotFoundError' do
        expect {
          business.find(-1)
        }.to raise_error(CampaignNotFound)
      end
    end
  end

  describe '#delete' do
    let!(:campaign) { FactoryGirl.create(:campaign) }

    it 'deletes the campaign' do
      expect {
        business.delete(campaign.id)
      }.to change { Campaign.count }.by(-1)
    end

    context 'when the campaign does not exist' do
      it 'raise CampaignNotFoundError' do
        expect {
          business.delete(-1)
        }.to raise_error(CampaignNotFound)
      end
    end
  end

  describe '#update' do
    let(:branch)         { FactoryGirl.create(:branch) }
    let(:campaign)       { FactoryGirl.create(:campaign) }
    let(:title)          { 'Title' }
    let(:offline)        { true }

    let(:attrs) do
      {
        'title'        => title,
        'branches_ids' => [branch.id],
        'offline'      => offline
      }
     end

    it 'updates a campaign' do
      expect {
        business.update(campaign.id, attrs)
        campaign.reload
      }.to change { campaign.title }.to(title)
    end

    it 'updates the campaign branches' do
      business.update(campaign.id, attrs)
      campaign.reload
      expect(campaign.branches).to include branch
    end

    it 'set de campaign to offline' do 
       expect {
        business.update(campaign.id, attrs)
        campaign.reload
      }.to change { campaign.offline? }.to(offline)
    end

    context 'when the campaign does not exist' do
      it 'raises campaignNotFound' do
        expect {
          business.update(-1, attrs)
        }.to raise_error(CampaignNotFound)
      end
    end
  end
end