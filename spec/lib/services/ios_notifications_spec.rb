require 'spec_helper'

require 'lib/services/ios_notifications'

describe Services::IOSNotifications do
  subject(:notification_service) { Services::IOSNotifications.new }

  describe '#send_notification' do
    let(:device_token) { '45d4b93c96c050d3' }
    let(:message) { FactoryGirl.create(:message) }

    it 'sends the message through the APNS' do
      expect(APNS).to receive(:send_notification).with(
        device_token,
        alert: message.title,
        badge: 1,
        sound: 'default',
        other: { campaign_id: message.campaign_id, message_id: message.id }
      )

      notification_service.send_notification(device_token, message)
    end
  end
end