require 'spec_helper'

require 'lib/services/android_notifications'

describe Services::AndroidNotifications do
  subject(:notification_service) { Services::AndroidNotifications.new }

  describe '#send_notification' do
    let(:device_token) { 'APA91bFofjyoExPuULRXFND3gL2q' }
    let(:message) { FactoryGirl.create(:message) }

    it 'sends the message through the APNS' do
      expect(GCM).to receive(:send_notification).with(
        device_token, 
        title: message.title, body: message.body, campaign_id: message.campaign_id, message_id: message.id
      )

      notification_service.send_notification(device_token, message)
    end
  end
end