module Representations
  module UserBeaconInteraction
    def self.user_beacon_interaction(user_beacon_interaction)
      {
        'id'          => user_beacon_interaction.id,
        'beacon_id'   => user_beacon_interaction.beacon_id,
        'user_id'     => user_beacon_interaction.user_id,
        'message'     => user_beacon_interaction.message.as_json,
        'at'          => user_beacon_interaction.at.utc.iso8601(3)
      }
    end
  end
end
