module Representations
  module Beacon
    def self.beacon(beacon)
      {
        'id'        => beacon.id,
        'name'      => beacon.name,
        'uuid'      => beacon.uuid,
        'major'     => beacon.major,
        'minor'     => beacon.minor,
        'branch_id' => beacon.branch_id,
        'string_id' => beacon.string_id
      }
    end
  end
end