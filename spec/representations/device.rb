module Representations
  module Device
    def self.device(device)
      {
        'id'          => device.id,
        'token'       => device.token,
        'platform'    => device.platform,
        'version'     => device.version,
        'environment' => device.environment,
        'user_id'     => device.user_id
      }
    end
  end
end