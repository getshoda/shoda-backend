module Representations
  module Interest
    def self.interest(interest)
      {
        'id'        => interest.id,
        'name'      => interest.name
      }
    end
  end
end