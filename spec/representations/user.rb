module Representations
  module User
    def self.user(user)
      birth_date = user.birth_date ? user.birth_date.to_s : nil

      {
        'id'              => user.id,
        'first_name'      => user.first_name,
        'last_name'       => user.last_name,
        'email'           => user.email,
        'gender'          => user.gender,
        'birth_date'      => birth_date,
        'locale'          => user.locale,
        'facebook_id'     => user.facebook_id,
        'facebook_token'  => user.facebook_token,
        'created_at'      => user.created_at.utc.iso8601(3),
        'updated_at'      => user.updated_at.utc.iso8601(3),
        'twitter_id'      => user.twitter_id,
        'twitter_token'   => user.twitter_token,
        'username'        => user.username
      }
    end
  end
end