module Representations
  module Branch
    def self.branch(branch)
      {
        'id'              => branch.id,
        'name'            => branch.name,
        'description'     => branch.description,
        'enterprise_id'   => branch.enterprise_id,
        'address_line_1'  => branch.address_line_1,
        'address_line_2'  => branch.address_line_2,
        'postal_code'     => branch.postal_code,
        'city'            => branch.city,
        'state'           => branch.state,
        'country'         => branch.country,
        'latitude'        => branch.latitude,
        'longitude'       => branch.longitude,
        'phone'           => branch.phone,
        'website'         => branch.website,
        'open_from'       => branch.open_from,
        'open_to'         => branch.open_to,
        'image_url'       => branch.image_url
      }
    end
  end
end