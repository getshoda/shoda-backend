module Representations
  module Enterprise
    def self.enterprise(enterprise)
      {
        'id'                    => enterprise.id,
        'name'                  => enterprise.name,
        'description'           => enterprise.description,
        'website'               => enterprise.website,
        'logo_url'              => enterprise.logo_url,
        'phone'                 => enterprise.phone,
        'tags'                  => enterprise.tags,
        'open_from'             => enterprise.open_from,
        'open_to'               => enterprise.open_to,
        'background_image_url'  => enterprise.background_image_url,
        'branches_count'        => enterprise.branches.size
      }
    end
  end
end