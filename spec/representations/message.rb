module Representations
  module Message
    def self.message(message)
      {
        'id'                    => message.id,
        'title'                 => message.title,
        'body'                  => message.body,
        'image_url'             => message.image_url,
        'video_url'             => message.video_url,
        'terms_and_conditions'  => message.terms_and_conditions,
        'campaign_id'           => message.campaign_id,
        'rules'                 => message.rules,
        'content'               => message.content,
        'ttl'                   => message.ttl,
        'welcome'               => message.welcome?
      }
    end
  end
end