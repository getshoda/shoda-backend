module Representations
  module Campaign
    def self.campaign(campaign)
      {
        'id'            => campaign.id,
        'title'         => campaign.title,
        'from'          => campaign.from.utc.iso8601(3),
        'to'            => campaign.to.utc.iso8601(3),
        'enterprise_id' => campaign.enterprise_id,
        'offline'       => campaign.offline?,
        'branches_ids'  => campaign.branches.map(&:id)
      }
    end
  end
end