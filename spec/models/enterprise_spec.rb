require 'spec_helper'

describe Enterprise do
  it_behaves_like 'a query-able model'
  
  describe 'validations' do
    context 'when no name is give' do

      it 'raises ActiveRecord::RecordInvalid' do
        expect { 
          Enterprise.create!(name: nil)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankNameError/)
      end
    end
  end
end