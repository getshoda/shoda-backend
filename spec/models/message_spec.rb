require 'spec_helper'

describe Message do
  describe 'validations' do
    let(:campaign) { FactoryGirl.create(:campaign) }
    let(:attributes) { { title: 'some title', body: 'some body', campaign_id: campaign.id } }

    context 'when no title is give' do
      before { attributes[:title] = nil }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Message.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankTitleError/)
      end
    end

    context 'when no body is give' do
      before { attributes[:body] = nil }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Message.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankBodyError/)
      end
    end

    context 'when the campaign is not given' do
      before { attributes[:campaign_id] = nil }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Message.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /InvalidCampaignIDError/)
      end
    end
  end

  describe '#enabled?' do
    let(:campaign) { FactoryGirl.create(:campaign) }
    let(:message)  { FactoryGirl.create(:message, campaign: campaign) }

    subject { message.enabled? }

    it { is_expected.to be }

    context 'when its campaign does not exist' do
      before { message.update_attribute(:campaign, nil) }

      it { is_expected.to_not be }
    end

    context 'when the campaign expired' do
      let(:campaign) { FactoryGirl.create(:campaign, :expired) }

      it { is_expected.to_not be }
    end

    context 'when the campaign has not started' do
      let(:campaign) { FactoryGirl.create(:campaign, :expired) }

      it { is_expected.to_not be }
    end
  end

  describe '.welcome_by_enterprise' do
    let(:campaign) { FactoryGirl.create(:campaign) }
    let(:message)  { FactoryGirl.create(:message, campaign: campaign, welcome: true) }

    subject { Message.welcome_by_enterprise(campaign.enterprise_id) }

    it { is_expected.to include message }

    context 'when the enterprise has no welcome message' do
      before { message.update_attribute(:welcome, false) }

      it { is_expected.to be_empty }
    end
  end
end
