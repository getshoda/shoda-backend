require 'spec_helper'

describe User do
  
  describe 'user\'s password' do
    context 'when there user doesn\'t have password' do
      let(:user) { User.new(first_name: 'Bruce', last_name: 'Wayne', has_password: false) }

      it 'is valid' do
        expect(user).to be_valid
      end

      it 'has no password' do
        expect(user.has_password).to eql false
      end

      it 'authentication should not succeed' do
        expect(user.authenticate('foobar')).to eql false
      end

      context 'and has_password is not set' do
        before do
          user.has_password = nil
        end

        it 'is invalid' do
          expect(user).to be_invalid
        end
      end
    end

    context 'when the user has a password' do
      let(:password) { 'foobar' }
      let(:user) { User.new(first_name: 'Bruce', last_name: 'Wayne', password: password) }

      it 'is valid' do
        expect(user).to be_valid
      end

      it 'has password' do
        expect(user.has_password).to eql true
      end

      it 'authentication succeeds' do
        expect(user.authenticate(password)).to eql true
      end
    end
  end

  describe 'validations' do
    let(:user_attrs) {{ first_name: 'Matt', last_name: 'Murdock' }}

    context 'with invalid gender' do
      before { user_attrs[:gender] = 'invalid' }

      it 'raises error' do
        expect { 
          User.create!(user_attrs)
        }.to raise_error(ActiveRecord::RecordInvalid, /InvalidGender/)
      end
    end
  end

  describe 'after_create' do
    let(:user) { FactoryGirl.create(:user) }

    it 'has salt' do
      expect(user.salt).to be
    end
  end

  describe '#interest_names' do
    let(:batman)   { FactoryGirl.create(:interest, name: 'batman') }
    let(:flash)    { FactoryGirl.create(:interest, name: 'flash') }
    let(:superman) { FactoryGirl.create(:interest, name: 'superman') }

    let(:user) { FactoryGirl.create(:user, interests: [batman, flash]) }

    subject { user.interest_names }

    it { is_expected.to include 'batman' }
    it { is_expected.to include 'flash' }
    it { is_expected.to_not include 'superman' }

    context 'when the user has no interests' do
      let(:user) { FactoryGirl.create(:user, interests: []) }

      it { is_expected.to be_empty }
    end
  end

  describe '#age' do
    let(:user) { FactoryGirl.create(:user) }

    subject { user.age }

    before do 
      allow(Time).to receive(:now) { DateTime.new(2015, 3, 10) }
    end

    it { is_expected.to be 23 }

    context 'when the user \'s birth date is unknown' do
      let(:user) { FactoryGirl.create(:user, birth_date: nil) }

      it { is_expected.to_not be }
    end

    context 'one day after the user\'s birthday' do
      before do 
        allow(Time).to receive(:now) { DateTime.new(2015, 7, 8) }
      end      

      it { is_expected.to be 24 }
    end
  end
end