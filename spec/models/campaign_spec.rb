require 'spec_helper'

describe Campaign do
  describe '#active?' do
    let(:campaign) { FactoryGirl.create(:campaign) }

    subject { campaign.active? }

    context 'within the valid dates' do
      it { is_expected.to be }
    end

    context 'when the campaign expired' do
      let(:campaign) { FactoryGirl.create(:campaign, :expired) }

      it { is_expected.to_not be }
    end

    context 'when the campaign has not yet started' do
      let(:campaign) { FactoryGirl.create(:campaign, :not_started) }

      it { is_expected.to_not be }
    end

    context 'when the campaign has no expiration date' do
      let(:campaign) { FactoryGirl.create(:campaign, from: nil) }

      it { is_expected.to be }

      context 'and the actual date is before the start date' do
        let(:campaign) { FactoryGirl.create(:campaign, :not_started) }

        it { is_expected.to_not be }
      end
    end

    context 'when the campaign has no satart date' do
      let(:campaign) { FactoryGirl.create(:campaign, to: nil) }

      it { is_expected.to be }

      context 'and the actual date is after the expiration date' do
        let(:campaign) { FactoryGirl.create(:campaign, :expired) }

        it { is_expected.to_not be }
      end
    end
  end

  describe 'validations' do
    let(:attributes) {{ title: 'Some title', enterprise_id: FactoryGirl.create(:enterprise).id }}

    context 'when the title is blank' do
      before { attributes.delete(:title) }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Campaign.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankTitleError/)
      end
    end

    context 'when the enterprise id is blank' do
      before { attributes.delete(:enterprise_id) }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Campaign.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankEnterpriseIDError/)
      end
    end
  end
end