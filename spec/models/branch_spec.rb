require 'spec_helper'

describe Branch do
  describe 'validations' do
    let(:enterprise) { FactoryGirl.create(:enterprise) }
    let(:attributes) { { name: 'Coolest branch', enterprise: enterprise } }

    context 'when no name is given' do
      before { attributes.delete(:name) }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Branch.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankNameError/)
      end
    end

    context 'when no enterprise is given' do
      before { attributes.delete(:enterprise) }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Branch.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankEnterpriseID/)
      end
    end

    context 'when the enterprise given does not exist' do
      before do 
        attributes.delete(:enterprise)
        attributes[:enterprise_id] = -1
      end

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Branch.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /InvalidEnterpriseID/)
      end
    end

    context 'when the latitude is less than -90' do
      before { attributes[:latitude] = -90.01 }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Branch.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /LatitudeValueError/)
      end
    end

    context 'when the latitude is more than 90' do
      before { attributes[:latitude] = 90.01 }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Branch.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /LatitudeValueError/)
      end
    end

    context 'when the longitude is less than -90' do
      before { attributes[:longitude] = -180.01 }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Branch.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /LongitudeValueError/)
      end
    end

    context 'when the longitude is more than 90' do
      before { attributes[:longitude] = 180.01 }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Branch.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /LongitudeValueError/)
      end
    end
  end

end