require 'spec_helper'

describe Beacon do
  it_behaves_like 'a query-able model'
  
  let(:branch)     { FactoryGirl.create(:branch) }
  let(:attributes) { { name: 'Coolest beacon', branch: branch, uuid: 'nanaBatman' } }

  describe 'validations' do
    context 'when no name is given' do
      before { attributes.delete(:name) }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Beacon.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankNameError/)
      end
    end

    context 'when no branch is given' do
      before { attributes.delete(:branch) }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Beacon.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankBranchID/)
      end
    end

    context 'when the branch given does not exist' do
      before do 
        attributes.delete(:branch)
        attributes[:branch_id] = -1
      end

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Beacon.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /InvalidBranchID/)
      end
    end

    context 'when no uuid is given' do
      before { attributes.delete(:uuid) }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Beacon.create!(attributes)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankUUID/)
      end
    end
  end

  describe 'after create' do
    subject { Beacon.create!(attributes) }

    its(:string_id) do 
      expected = [Beacon::STRING_ID_ROOT, branch.enterprise_id, branch.id, subject.id].join('.')
      
      is_expected.to eql expected
    end
  end

  describe '.discover' do
    let(:beacon) { FactoryGirl.create(:beacon) }

    it 'gets the beacon with the given major, minor and uuid' do
      expect(Beacon.discover(beacon.major, beacon.minor, beacon.uuid)).to eql beacon
    end

    context 'when the beacon does not exist' do
      it 'returns nil' do
        expect(Beacon.discover(-1, -1, 'invalid')).to eql nil
      end
    end
  end
end