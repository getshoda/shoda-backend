require 'spec_helper'

describe Device do

  describe 'validations' do

    let(:user)  { FactoryGirl.create(:user) }

    let(:attrs) { { platform: 'ios', token: 'HeLLoThiSISaToKEN', user_id: user.id } }

    context 'when the platform given is invalid' do
      before { attrs[:platform] = 'WindowsPhone' }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Device.create!(attrs)
        }.to raise_error(ActiveRecord::RecordInvalid, /InvalidPlatform/)
      end
    end

    context 'when no token is given' do
      before { attrs.delete(:token) }
      
      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Device.create!(attrs)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankTokenError/)
      end
    end

    context 'when the user is not given' do
      before { attrs.delete(:user_id) }

      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Device.create!(attrs)
        }.to raise_error(ActiveRecord::RecordInvalid, /BlankUserID/)
      end
    end

    context 'when the user given does not exist' do
      before { attrs[:user_id] = -1 }
      
      it 'raises ActiveRecord::RecordInvalid' do
        expect {
          Device.create!(attrs)
        }.to raise_error(ActiveRecord::RecordInvalid, /InvalidUser/)
      end
    end
  end

  describe '#notify' do
    let(:message) { FactoryGirl.create(:message) }

    context 'with an android device' do
      let(:device) { FactoryGirl.create(:device, :android) }

      it 'sends an android push notification' do
        expect(
          Device.android_notification_server
        ).to receive(:send_notification).with(device.token, message)

        device.notify(message)
      end
    end

    context 'with an ios device' do
      let(:device) { FactoryGirl.create(:device, :ios) }

      it 'sends an ios push notification' do
        expect(
          Device.ios_notification_server
        ).to receive(:send_notification).with(device.token, message)

        device.notify(message)
      end
    end
  end
end