require 'spec_helper'

describe UserBeaconInteraction do
  it_behaves_like 'a query-able model'

  describe '#is_a_new_visit?' do
    let(:interaction) { FactoryGirl.create(:user_beacon_interaction) }

    subject { interaction.is_a_new_visit? }

    context 'without a previous interaction' do
      it { is_expected.to be true }
    end

    context 'with a previous interaction' do
      let!(:previous_interaction) do
        FactoryGirl.create(
          :user_beacon_interaction, beacon: interaction.beacon, at: at
        )
      end

      context 'when the previous interaction was a long time ago' do
        let(:at) { interaction.at - 8.hours }

        it { is_expected.to be true }
      end

      context 'when the previous interaction was a while ago' do
        let(:at) { interaction.at - 1.hours }

        it { is_expected.to be false }
      end
    end
  end
end
