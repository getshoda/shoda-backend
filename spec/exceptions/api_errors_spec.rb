require 'spec_helper'

describe ResourceInvalid do
  describe 'a ResourceInvalid error' do
    let(:error_messages) do
      {
        attribute: ['FirstError - First error']
      }
    end

    subject(:error) { ResourceInvalid.new(error_messages) }

    its(:key)     { is_expected.to eql('ResourceInvalid') }
    its(:message) { is_expected.to eql('FirstError - First error') }

    context 'when more than one attribute is invalid' do
      before { error_messages[:other_attribute] = 'OtherAttributeError - Second error' }

      its(:message) { is_expected.to match(/OtherAttributeError/) }
      its(:message) { is_expected.to match(/FirstError/) }
    end

    context 'when an attribute has more than one error' do
      before { error_messages[:attribute] << 'SecondError - Second error' }

      its(:message) { is_expected.to match(/SecondError/) }
      its(:message) { is_expected.to match(/FirstError/) }
    end
  end
end