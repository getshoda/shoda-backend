require 'rspec/expectations'
require 'json'

Dir['spec/representations/*.rb'].each { |file| require file }

RSpec::Matchers.define :match_representation do |representation, object, *options|
  rep_module = Object.const_get("Representations::#{object.class.name}")

  object.reload
  expected = rep_module.send(representation, object)

  match do |actual|
    get_representation(actual, representation, options) == expected
  end

  failure_message do |actual|
    "expected that #{get_representation(actual, representation, options)} would match #{expected}"
  end
end

RSpec::Matchers.define :be_an_empty_representation do |set_key|
  match do |actual|
    set = fetch_with_indifferent_access(JSON(actual.body), set_key)
    set == []
  end

  failure_message do |actual|
    "expected that #{fetch_with_indifferent_access(JSON(actual.body), set_key)} would be []"
  end
end

RSpec::Matchers.define :be_success do
  match do |actual|
    [200, 201].include? actual.status
  end

  failure_message do |actual|
    "expected that #{actual.status} would be 200 or 201"
  end
end

RSpec::Matchers.define :be_not_found do
  match do |actual|
    actual.status == 404
  end

  failure_message do |actual|
    "expected that #{actual.status} would be 404"
  end
end

RSpec::Matchers.define :be_unprocessable_entity do
  match do |actual|
    actual.status == 422
  end

  failure_message do |actual|
    "expected that #{actual.status} would be 422"
  end
end

RSpec::Matchers.define :be_unauthorized do
  match do |actual|
    actual.status == 401
  end

  failure_message do |actual|
    "expected that #{actual.status} would be 401"
  end
end

RSpec::Matchers.define :be_conflict do
  match do |actual|
    actual.status == 409
  end

  failure_message do |actual|
    "expected that #{actual.status} would be 409"
  end
end

RSpec::Matchers.define :be_conflict do
  match do |actual|
    actual.status == 409
  end

  failure_message do |actual|
    "expected that #{actual.status} would be 409"
  end
end

RSpec::Matchers.define :be_valid do
  match do |actual|
    actual.valid?
  end

  failure_message do |actual|
    if actual.is_a?(ActiveRecord::Base)
      "expected that model would be valid"
    else
      "expected that given object would be an instance of a model"
    end
  end
end

RSpec::Matchers.define :be_invalid do
  match do |actual|
    actual.invalid?
  end

  failure_message do |actual|
    if actual.is_a?(ActiveRecord::Base)
      "expected that model would be invalid"
    else
      "expected that given object would be an instance of a model"
    end
  end
end

RSpec::Matchers.define :contain_element do |element_key|
  match do |actual|
    fetch_with_indifferent_access(JSON(actual.body), element_key)
  end

  failure_message do |actual|
    "expected that #{actual.body} would contain element #{element_key}"
  end
end

RSpec::Matchers.define :contain_error do |error_key|
  match do |actual|
    error_element = fetch_with_indifferent_access(JSON(actual.body), :error)

    if error_key
      if error_element
        error_description = fetch_with_indifferent_access(error_element, error_key)
        error_description && error_description.match(error_key)
      end
    else
      error_element
    end
  end

  failure_message do |actual|
    key_failure_message = " and would match #{error_key}" if error_key
    "expected that #{JSON(actual.body)} would contain 'error'" + key_failure_message
  end
end

def get_representation(actual, representation, options)
  body = JSON(actual.body)

  if options.empty?
    fetch_with_indifferent_access(body, representation)
  else
    rep = options.inject(body) do |body, option|
      fetch_with_indifferent_access(body, option)
    end

    rep.kind_of?(Array) ? rep.last : rep
  end
end

def fetch_with_indifferent_access(hash, key)
  hash[key] || hash[key.to_s]
end
