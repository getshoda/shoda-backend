module Support
  
  module RoutesHelper
    module_function

    def signup_path
      '/signup'
    end

    def login_path
      '/login'
    end

    def facebook_login_path
      login_path + '/facebook'
    end

    def twitter_login_path
      login_path + '/twitter'
    end

    def logout_oath
      '/logout'
    end

    def enterprises_path
      full_path('/enterprises')
    end

    def enterprise_path(*args)
      id = args.first

      full_path("/enterprises/#{id}")
    end
    
    def branches_path
      full_path('/branches')
    end
     
    def branch_path(*args)
      id = args.first
  
      full_path("/branches/#{id}")
    end

    def branches_by_campaign_path
      "#{branches_path}/by_campaign"
    end
    
    def beacons_path
      full_path('/beacons')
    end

    def beacon_path(*args)
      id = args.first

      full_path("/beacons/#{id}")
    end

    def users_path
      full_path('/users')
    end

    def users_me_path
      full_path('/users/me')
    end

    def user_path(*args)
      id = args.first

      full_path("/users/#{id}")
    end

    def devices_path(*args)
      user_id = args.first

      full_path("/users/#{user_id}/devices")
    end

    def device_path(*args)
      user_id, id = args

      full_path("/users/#{user_id}/devices/#{id}")
    end

    def campaigns_path
      full_path('/campaigns')
    end

    def campaign_path(*args)
      id = args.first

      full_path("/campaigns/#{id}")
    end

    def message_path(*args)
      campaign_id, id = args

      campaign_path(campaign_id) + "/messages/#{id}"
    end

    def messages_path(*args)
      campaign_id = args.first
      
      campaign_path(campaign_id) + '/messages'
    end

    def user_interactions_path(*args)
      user_id = args.first

      full_path("/users/#{user_id}/interactions")
    end

    def interests_path
      full_path('/interests')
    end

    def user_interests_path(*args)
      user_path(*args) + '/interests'
    end

    def base_path
      "/#{app.api_version}"
    end

    def full_path(path)
      base_path + path
    end
    private_class_method :base_path, :full_path
  end
end