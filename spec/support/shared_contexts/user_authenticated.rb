RSpec.shared_context 'user authenticated' do

  let(:current_user) { FactoryGirl.create(:user, email: 'batman@shoda.com') }

  before do
    header('Authorization', 'woop woop')
    allow_any_instance_of(Business::Sessions).to receive(:find_user_with_session) { current_user }
  end
end
