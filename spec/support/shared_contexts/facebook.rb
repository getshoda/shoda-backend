RSpec.shared_context 'facebook successful authentication' do 
  let(:token) { rand(1_000_000).to_s }
  let(:facebook_client) { double(:facebook_client, get_object: user_attrs) }
  let(:user_attrs) { { first_name: 'Barry', last_name: 'Allen' } }

  before do
    allow(Koala::Facebook::API).to receive(:new).with(token) { facebook_client }
  end
end

RSpec.shared_context 'facebook failed authentication' do
  let(:authentication_error) { Koala::Facebook::AuthenticationError.new('error', 'error') }

  before do
    allow(Koala::Facebook::API).to receive(:new).with(token) { facebook_client }
    allow(facebook_client).to receive(:get_object).and_raise(authentication_error)
  end
end