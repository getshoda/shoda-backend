RSpec.shared_context 'twitter successful authentication' do
  let(:screen_name) { 'batman42' }
  let(:id)          { rand(1_000) }

  let(:twitter_client) do 
    double(:twitter_client, user: double(:user, id: id, screen_name: screen_name))
  end

  before do
    allow(Twitter::REST::Client).to receive(:new) { twitter_client }
  end
end

RSpec.shared_context 'twitter failed authentication' do
  before do
    allow(Twitter::REST::Client.new).to receive(:user).and_raise(Twitter::Error::BadRequest)
  end
end