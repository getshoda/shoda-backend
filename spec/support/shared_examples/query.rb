RSpec.shared_examples 'a query-able model' do #Yup, better name is needed :D

  let!(:first_model)  { FactoryGirl.create(described_class.to_s.underscore) }
  let!(:second_model) { FactoryGirl.create(described_class.to_s.underscore) }

  let(:model) { described_class }

  describe '#paginate' do
    let(:pagination) { { page: 1, per_page: 1 } }

    it 'does not return the elements of another page' do
      expect(model.paginate(pagination)).to include first_model
      expect(model.paginate(pagination)).to_not include second_model
    end

    it 'returns per_page number of elements' do
      expect(model.paginate(pagination).size).to eq pagination[:per_page]
    end

    context 'when the given page does not have any records' do
      let(:pagination) { { page: 3, per_page: 1 } }

      it 'returns an empty set' do
        expect(model.paginate(pagination)).to be_empty
      end
    end
  end

  describe '#filter' do
    let(:filter_params) { {} }

    context 'when no filter given' do
      it 'returns all the records' do
        expect(model.filter(filter_params)).to eq model.all
      end
    end

    context 'with eq filter' do
      let(:filter_params) { { id__eq: first_model.id } }

      it 'returns the records whose attribute equals the given value' do
        expect(model.filter(filter_params)).to include first_model
      end

      it 'does not return the records not matching the given value' do
        expect(model.filter(filter_params)).to_not include second_model
      end
    end

    context 'with in filter' do
      let(:filter_params) { { id__in: [second_model.id] } }

      it 'returns the records whose attributes are in the given values' do
        expect(model.filter(filter_params)).to include second_model
      end

      it 'does not returns the records not in the given values' do
        expect(model.filter(filter_params)).to_not include first_model
      end
    end

    context 'when like filter' do
      # This one is tricky (and hacky)

      let(:attribute)     { model.columns.find { |attr| attr.type == :string }.try(:name) }
      let(:filter_params) { { "#{attribute}__like" => 'HelloSweetie' } }

      before do
        if attribute
          first_model.update_attribute(attribute, 'Sweet')
          second_model.update_attribute(attribute, 'Nope')
        end
      end

      it 'returns the records whose values are like the given value' do
        skip unless attribute

        expect { model.filter(filter_params).to include first_model }
      end

      it 'does not returns the records whose attribute is not like the given value' do
        skip unless attribute

        expect { model.filter(filter_params).to_not include first_model }
      end
    end

    context 'when is null filter' do
      let(:attribute) { model.columns.find { |attr| attr.null == true }.try(:name) }
      let(:filter_params) { { "#{attribute}__null" => value } }

      before do
        first_model.update_attribute(attribute, nil)
        second_model.update_attribute(attribute, 'not null')
      end

      context 'when the value given is true' do
        let(:value) { true }

        it 'return the record with null value' do
          skip unless attribute

          expect { model.filter(filter_params).to include first_model }
        end

        it 'does not return the record with a set value' do
          skip unless attribute

          expect { model.filter(filter_params).to_not include second_model }
        end
      end

      context 'when the value given is false' do
        it 'returns the record with a set value' do
          skip unless attribute

          expect { model.filter(filter_params).to include second_model }
        end

        it 'does not return the record with null value' do
          skip unless attribute

          expect { model.filter(filter_params).to_not include first_model }
        end
      end
    end

    context 'when the given attribute is invalid' do
      let(:filter_params) { { invalid_stuff__eq: 'fruit' } }

      it 'raises InvalidAttribute' do
        expect {
          model.filter(filter_params)
        }.to raise_error(InvalidAttribute)
      end
    end

    context 'when the operation is invalid' do
      let(:filter_params) { { id__invalid_stuff: 'fruit' } }

      it 'raises InvalidAttribute' do
        expect {
          model.filter(filter_params)
        }.to raise_error(InvalidOperation)
      end
    end
  end
end
