require 'rubygems'
require 'bundler/setup'

require 'config/initializers/environment'
require 'config/initializers/configuration'
require 'config/initializers/db'
require 'lib/utils/query'
 
Bundler.require :default, RACK_ENV
 
require 'api/oswald'