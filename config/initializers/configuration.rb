require 'oswald_o'
require 'yaml'

CONFIGURATION = OswaldO.new(YAML::load(File.open('config/configuration.yml')))