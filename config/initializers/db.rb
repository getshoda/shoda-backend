require 'erb'
require 'yaml'
require 'active_record'

dbconfig = YAML.load(ERB.new(
  File.read(File.join('config', 'database.yml'))
).result)[RACK_ENV]

ActiveRecord::Base.establish_connection(dbconfig)