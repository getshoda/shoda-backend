$: << (File.dirname(__FILE__))

require 'rack/cors'
require File.expand_path('../config/boot', __FILE__)

use ActiveRecord::ConnectionAdapters::ConnectionManagement

require 'api/oswald'

use Rack::Cors do
  allow do
    origins /https?:\/\/localhost(:\d+)?/,
            /https?:\/\/.*\.getshoda\.com(:\d+)?/
    resource '*',
             :headers => :any,
             :methods => [:get, :post, :put, :delete, :options]
  end
end

run Rack::URLMap.new '/oswald' => Oswald
