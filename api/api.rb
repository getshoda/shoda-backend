require 'grape'

Dir['resources/*.rb'].each { |file| require file }
Dir['exceptions/*.rb'].each { |file| require file }
Dir['models/*.rb'].each { |file| require file }

class Api < Grape::API
  version 'v1', using: :path
  format :json

  before do
    authenticated?
    validate_required_params
  end

  after do
    ActiveRecord::Base.clear_active_connections!
  end

  helpers do
    def authenticated?
      raise InvalidUser if current_user.nil?
    end

    def current_user
      header = headers['Authorization']
      _type, token = header.split
      session_business.find_user_with_session(token)
    rescue
      raise InvalidUser
    end

    def session_business
      @session_business ||= Business::Sessions.new
    end
  end

  mount Resources::Enterprises
  mount Resources::Branches
  mount Resources::Beacons
  mount Resources::Users
  mount Resources::Campaigns
  mount Resources::Interests
end
