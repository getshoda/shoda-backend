require 'grape'

require 'lib/business/sessions'
require 'lib/business/users'
require 'exceptions/oswald_error'

class Auth < Grape::API
  format :json

  helpers do
    def users_business
      @users_business ||= Business::Users.new
    end

    def session_business
      @session_business ||= Business::Sessions.new
    end
  end

  desc 'Logs in and creates JWT token'
  params do
    requires :email, type: String, desc: 'The user\'s email address.'
    requires :password, type: String, desc: 'The user\'s password'
  end
  post :login do
    user = users_business.login_with_email(params[:email], params[:password])

    { token: session_business.get_session(user) }
  end

  desc 'Logs in with Facebook and creates JWT token'
  params do
    requires :fb_user_id, type: String, desc: 'The user\'s facebook id.'
    requires :access_token, type: String, desc: 'The user\'s facebook access token.'
  end
  post 'login/facebook' do
    user = users_business.login_with_facebook(params[:fb_user_id], params[:access_token])
    
    { token: session_business.get_session(user) }
  end

  desc 'Logs in with Twitter and creates JWT token'
  params do
    requires :access_token, type: String, desc: 'The user\'s twitter access token.'
    requires :access_secret, type: String, desc: 'The user\'s twitter access secret.'
  end
  post 'login/twitter' do
    user = users_business.login_with_twitter(params[:access_token], params[:access_secret])
    
    { token: session_business.get_session(user) }
  end

  desc 'Logs out and destroys JWT token'
  post :logout do
    token = headers['Authorization'].split.last
    session_business.destroy_session(token)

    body false
    status 204
  end

  desc 'Signs up with a new user that uses email and password and returns a JWT token'
  params do
    requires :email, type: String, desc: 'The new user\'s email address.'
    requires :password, type: String, desc: 'The new user\'s password.'
  end
  post :signup do
    user = users_business.create(whitelisted_params)
    
    { token: session_business.get_session(user) }
  end
end