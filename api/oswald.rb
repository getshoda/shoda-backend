require 'grape'

require 'api/auth'
require 'api/api'

class Oswald < Grape::API

  rescue_from :all do |e|
    logger = Logger.new('log/errors.log')
    logger.error e
    raise
  end

  rescue_from OswaldError do |e|
    rack_response e.to_json, e.status
  end

  def Oswald.api_version
    Api.version
  end

  helpers do
    def whitelisted_params
      params.to_hash.select { |key, _value| route.route_params.keys.include?(key) }
    end

    def validate_required_params
      required_params = route.route_params.map do |key, value|
        key if value.is_a?(Hash) and value[:required]
      end.compact

      missing_params = required_params.reject { |required_param| params.include?(required_param) }

      raise MissingParameter.new(missing_params) unless missing_params.empty?
    end
  end

  mount Auth
  mount Api
end
