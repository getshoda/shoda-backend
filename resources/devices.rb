require 'lib/business/devices'

module Resources
  class Devices < Grape::API
    
    resource :devices do

      helpers do
        def devices_business
          @devices_business ||= Business::Devices.new
        end
      end

      desc 'Creates a new device.'
      params do
        requires :token, type: String, desc: 'The device\'s token.'
        requires :platform, type: String, desc: 'The device\'s token.'
        requires :version, type: String, desc: 'The devive os version.'
        optional :environment, type: String, desc: 'The environment for the device.'
      end
      post do
        device = devices_business.create(
          params[:user_id], whitelisted_params
        ) 
        
        present :device, device
      end
    end
  end
end