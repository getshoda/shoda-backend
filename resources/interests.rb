require 'lib/business/interests'

module Resources
  class Interests < Grape::API

    resource :interests do

      helpers do
        def interests_business
          @interests_business ||= Business::Interests.new
        end
      end

      desc 'Gets a list of interests.'
      get do
        present :interests, interests_business.get
      end
    end
  end
end