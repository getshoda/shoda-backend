require 'lib/business/user_beacon_interactions'
require 'lib/utils/shared_params'

module Resources
  class Interactions < Grape::API

    resource :interactions do

      helpers SharedParams

      helpers do
        def interactions_business
          @interactions_business ||= Business::UserBeaconInteractions.new
        end
      end

      desc 'Get the list of interactions for the given user'
      params do
        use :pagination
        use :query, resource: :user_beacon_interaction
      end
      get do
        interactions = interactions_business.get(params[:user_id], whitelisted_params.except('user_id'))
        present :interactions, interactions
      end

      desc 'Creates a new interaction.'
      params do
        requires :major, type: Integer, desc: 'Beacons major'
        requires :minor, type: Integer, desc: 'Beacons minor'
        requires :uuid, type: String, desc: 'Beacons uuid'
        optional :type, type: Integer, desc: 'The type of the beacon.'
        optional :at, type: String, desc: 'The moment the interaction ocurred.'
      end
      post do
        user_beacon_interaction = interactions_business.create(
          params[:user_id], whitelisted_params
        )

        present :user_beacon_interaction, user_beacon_interaction
      end
    end
  end
end
