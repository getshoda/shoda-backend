require 'lib/business/campaigns'
require 'resources/messages'

module Resources
  class Campaigns < Grape::API

    resource :campaigns do

      helpers SharedParams
      
      helpers do
        def campaigns_business
          @campaigns_business ||= Business::Campaigns.new
        end
      end

      desc 'Creates a new campaign.'
      params do
        requires :title, type: String, desc: 'The campaigns title.'
        requires :enterprise_id, type: Integer, desc: 'The enterprise identifier.'
        optional :branches_ids, type: Array, desc: 'The branches for which the campaign is valid.'
        optional :from, type: String, desc: 'The date from which the campaign is active.'
        optional :to, type: String, desc: 'The date until the campaign is active.'
        optional :offline, type: Boolean, desc: 'Indicates if it is an offline campaign'
      end
      post do
        campaign = campaigns_business.create(
          whitelisted_params
        )

        present :campaign, campaign
      end

      desc 'Return a list of campaigns.'
      params do
        use :pagination
        use :query, resource: :beacon
      end
      get do
        present :campaigns, campaigns_business.get
      end

      route_param :id, desc: 'The campaign id.' do
        desc 'Gets a campaign by its id.'
        get do
          present :campaign, campaigns_business.find(params[:id])
        end

        desc 'Deletes a campaign.'
        delete do
          campaigns_business.delete(params[:id])
        end

        desc 'Updates a campaign.'
        params do
          optional :title, type: String, desc: 'The campaigns title.'
          optional :branches_ids, type: Array, desc: 'The branches for which the campaign is valid.'
          optional :from, type: String, desc: 'The date from which the campaign is active.'
          optional :to, type: String, desc: 'The date until the campaign is active.'
          optional :offline, type: Boolean, desc: 'Indicates if it is a offline campaign'
        end
        put do
          campaign = campaigns_business.update(
            params[:id],
            whitelisted_params
          )

          present :campaign, campaign
        end
      end

      route_param :campaign_id do
        mount Resources::Messages
      end
    end
  end
end
