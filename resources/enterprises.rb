require 'lib/business/enterprises'
require 'lib/utils/shared_params'

module Resources
  class Enterprises < Grape::API

    resource :enterprises do

      helpers SharedParams

      helpers do
        def enterprises_business
          Business::Enterprises.new
        end
      end

      desc 'Return a list of enterprises.'
      params do
        use :pagination
        use :query, resource: :enterprise
      end
      get do
        present :enterprises, enterprises_business.get(whitelisted_params)
      end

      desc 'Gets a enterprise.'
      params do
        requires :id, type: Integer, desc: 'The enterprise id.'
      end
      route_param :id do
        get do
          present :enterprise, enterprises_business.find(params[:id])
        end
      end

      desc 'Creates a new enterprise.'
      params do
        requires :name, type: String, desc: 'The enterprise name.'
        requires :description, type: String, desc: 'The enterprise description.'
        requires :website, type: String, desc: 'The enterprise website.'
        requires :logo_url, type: String, desc: 'The enterprise logo_url.'
        optional :tags, type: Array, desc: 'The enterprise tags.'
        optional :phone, type: String, desc: 'The enterprise phone number.'
        optional :open_from, type: String, desc: 'The enterprise opening hour.'
        optional :open_to, type: String, desc: 'The enterprise closing hour.'
        optional :background_image_url, type: String, desc: 'An enterprise background image url.'
      end
      post do
        enterprise = enterprises_business.create(
          whitelisted_params
        )

        present :enterprise, enterprise
      end

      desc 'Deletes a enterprise.'
      route_param :id do
        delete do
          enterprises_business.delete(params[:id])
        end
      end

      desc 'Updates a enterprise.'
      params do
        optional :name, type: String, desc: 'The enterprise name.'
        optional :description, type: String, desc: 'The enterprise description.'
        optional :website, type: String, desc: 'The enterprise website.'
        optional :logo_url, type: String, desc: 'The enterprise logo_url.'
        optional :tags, type: Array, desc: 'The enterprise tags.'
        optional :phone, type: String, desc: 'The enterprise phone number.'
        optional :open_from, type: String, desc: 'The enterprise opening hour.'
        optional :open_to, type: String, desc: 'The enterprise closing hour.'
        optional :background_image_url, type: String, desc: 'An enterprise background image url.'
      end
      route_param :id, desc: 'The enterprise id.' do
        put do
          enterprise = enterprises_business.update(
            params[:id],
            whitelisted_params
          )

          present :enterprise, enterprise
        end
      end
    end
  end
end
