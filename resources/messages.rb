require 'lib/business/messages'

module Resources
  class Messages < Grape::API

    resource :messages do

      helpers do
        def messages_business
          Business::Messages.new
        end
      end

      desc 'Return a list of messages.'
      get do
        present :messages, messages_business.get(params[:campaign_id])
      end

      desc 'Creates a new message.'
      params do
        requires :title, type: String, desc: 'The message title.'
        requires :body, type: String, desc: 'The message body.'
        optional :beacons_ids, type: Array, desc: 'The message beacons.'
        optional :image_url, type: String, desc: 'The message image url.'
        optional :video_url, type: String, desc: 'The message video url.'
        optional :terms_and_conditions, type: String, desc: 'The message terms and conditions.'
        optional :rules, type: Array, desc: 'The rules to trigger the message.'
        optional :content, type: Hash, desc: 'Content of the message as a hash.'
        optional :welcome, type: Boolean, desc: 'Indicates if it is a welcome message.'
      end
      post do
        message = messages_business.create(
          whitelisted_params
        )

        present :message, message
      end

      route_param :id do
        desc 'Gets a message.'
        params do
          requires :id, type: Integer, desc: 'The message id.'
        end
        get do
          present :message, messages_business.find(params[:id])
        end

        desc 'Deletes a message.'
        delete do
          messages_business.delete(params[:id])
        end

        desc 'Updates a message.'
        params do
          optional :title, type: String, desc: 'The message title.'
          optional :body, type: String, desc: 'The message body.'
          optional :image_url, type: String, desc: 'The message image url.'
          optional :video_url, type: String, desc: 'The message video url.'
          optional :terms_and_conditions, type: String, desc: 'The message terms and conditions.'
          optional :rules, type: Array, desc: 'The rules to trigger the message.'
          optional :content, type: Hash, desc: 'Content of the message as a hash.'
          optional :welcome, type: Boolean, desc: 'Indicates if it is a welcome message.'
        end
        put do
          message = messages_business.update(
            params[:id],
            whitelisted_params
          )

          present :message, message
        end
      end
    end
  end
end