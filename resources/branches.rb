require 'lib/business/branches'
require 'lib/utils/shared_params'

module Resources
  class Branches < Grape::API

    resource :branches do

      helpers SharedParams

      helpers do
        def branches_business
          Business::Branches.new
        end
      end

      desc 'Return a list of branches.'
      params do
        use :pagination
        use :query, resource: :branch
      end
      get do
        present :branches, branches_business.get(whitelisted_params)
      end

      desc 'Creates a new branch.'
      params do
        requires :name, type: String, desc: 'The branch name.'
        requires :description, type: String, desc: 'The branch description.'
        requires :address_line_1, type: String, desc: 'The branch address first line.'
        optional :address_line_2, type: String, desc: 'The branch address second line.'
        optional :postal_code, type: String, desc: 'The branch address postal code.'
        optional :city, type: String, desc: 'The branch address city.'
        optional :state, type: String, desc: 'The branch address state.'
        optional :country, type: String, desc: 'The branch address country.'
        optional :latitude, type: Float, desc: 'The branch latitude component of the coordinates.'
        optional :longitude, type: Float, desc: 'The branch longitude component of the coordinates.'
        optional :phone, type: String, desc: 'The branch phone number.'
        optional :website, type: String, desc: 'The branch website address.'
        optional :open_from, type: String, desc: 'The branch opening hour.'
        optional :open_to, type: String, desc: 'The branch closing hour.'
        optional :image_url, type: String, desc: 'A branch image url.'
        requires :enterprise_id, type: Integer, desc: 'The enterprise owner of the branch.'
      end
      post do
        branch = branches_business.create(
            whitelisted_params
        )

        present :branch, branch
      end

      desc 'Gets branches by a campaign id'
      params do
        requires :campaign_id, type: Integer, desc: 'The campaign id.'
      end
      get :by_campaign do
        { branches: branches_business.by_campaign(params[:campaign_id]) }
      end

      route_param :id do
        desc 'Gets a branch.'
        get do
          present :branch, branches_business.find(params[:id])
        end

        desc 'Deletes a branch.'
        delete do
          branches_business.delete(params[:id])
        end

        desc 'Updates a branch.'
        params do
          requires :name, type: String, desc: 'The branch name.'
          requires :description, type: String, desc: 'The branch description.'
          requires :address_line_1, type: String, desc: 'The branch address first line.'
          optional :address_line_2, type: String, desc: 'The branch address second line.'
          optional :postal_code, type: String, desc: 'The branch address postal code.'
          optional :city, type: String, desc: 'The branch address city.'
          optional :state, type: String, desc: 'The branch address state.'
          optional :country, type: String, desc: 'The branch address country.'
          optional :latitude, type: Float, desc: 'The branch latitude component of the coordinates.'
          optional :longitude, type: Float, desc: 'The branch longitude component of the coordinates.'
          optional :phone, type: String, desc: 'The branch phone number.'
          optional :website, type: String, desc: 'The branch website address.'
          optional :open_from, type: String, desc: 'The branch opening hour.'
          optional :open_to, type: String, desc: 'The branch closing hour.'
          optional :image_url, type: String, desc: 'A branch image url.'
        end
        put do
          branch = branches_business.update(
            params[:id],
            whitelisted_params
          )

          present :branch, branch
        end
      end
    end
  end
end