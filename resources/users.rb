require 'lib/business/users'
require 'resources/devices'
require 'resources/interactions'

module Resources
  class Users < Grape::API

    resource :users do

      helpers do
        def users_business
          @users_business ||= Business::Users.new
        end
      end

      desc 'Return a list of Users'
      get do
        present :users, users_business.get
      end

      desc 'Returns the user\'s profile'
      get :me do
        present :user, current_user
      end

      desc 'Creates a new user.'
      params do
        requires :first_name, type: String, desc: 'The users first name.'
        requires :last_name, type: String, desc: 'The users last name.'
        optional :gender, type: String, desc: 'The users gender.'
        optional :password, type: String, desc: 'The user password.'
        optional :birth_date, type: Date, desc: 'The users birth date.'
        optional :locale, type: String, desc: 'The users locale.'
        optional :email, type: String, desc: 'The users email.'
        optional :facebook_id, type: String, desc: 'The users facebook_id.'
      end
      post do
        user = users_business.create(
            whitelisted_params
        )

        present :user, user
      end

      desc 'Updates a user.'
      params do
        requires :first_name, type: String, desc: 'The users first name.'
        requires :last_name, type: String, desc: 'The users last name.'
        optional :gender, type: String, desc: 'The users gender.'
        optional :password, type: String, desc: 'The user password.'
        optional :birth_date, type: Date, desc: 'The users birth date.'
        optional :locale, type: String, desc: 'The users locale.'
        optional :email, type: String, desc: 'The users email.'
      end
      route_param :id, desc: 'The user id.' do
        put do
          user = users_business.update(
            params[:id],
            whitelisted_params.except('id')
          )

          present :user, user
        end
      end

      route_param :id do
        resource :interests do
          desc 'Gets an user interests.'
          get do
            user = users_business.find(params[:id])

            present :interests, user.interests
          end

          desc 'Updates an user interests.'
          params do
            optional :interests_ids, type: Array, desc: 'New user interests identifiers.', default: []
          end
          put do
            user = users_business.update(
              params[:id], whitelisted_params
            )

            present :user, user
          end
        end
      end

      route_param :user_id do
        mount Resources::Devices
        mount Resources::Interactions
      end
    end
  end
end