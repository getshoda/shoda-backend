require 'lib/business/beacons'
require 'lib/utils/shared_params'

module Resources
  class Beacons < Grape::API

    resource :beacons do

      helpers SharedParams

      helpers do
        def beacons_business
          @beacons_business ||= Business::Beacons.new
        end
      end

      desc 'Return a list of beacons.'
      params do
        use :pagination
        use :query, resource: :beacon
      end
      get do
        present :beacons, beacons_business.get(whitelisted_params)
      end

      desc 'Gets a beacon.'
      params do
        requires :id, type: Integer, desc: 'The beacon id.'
      end
      route_param :id do
        get do
          present :beacon, beacons_business.find(params[:id])
        end
      end

      desc 'Creates a new beacon.'
      params do
        requires :uuid, type: String, desc: 'The beacon uuid.'
        requires :branch_id, type: Integer, desc: 'The beacon\'s branch identifier.'
        requires :name, type: String, desc: 'The beacon name.'
        requires :major, type: Integer, desc: 'The beacon\'s major.'
        requires :minor, type: Integer, desc: 'The beacon\'s minor.'
      end
      post do
        present :beacon, beacons_business.create(whitelisted_params)
      end

      desc 'Deletes a beacon.'
      route_param :id do
        delete do
          beacons_business.delete(params[:id])
        end
      end

      desc 'Updates a beacon.'
      params do
        optional :uuid, type: String, desc: 'The beacon uuid.'
        optional :name, type: String, desc: 'The beacon name.'
        optional :major, type: Integer, desc: 'The beacon\'s major.'
        optional :minor, type: Integer, desc: 'The beacon\'s minor.'
      end
      route_param :id, desc: 'The beacon id.' do
        put do
          beacon = beacons_business.update(
            params[:id],
            whitelisted_params
          )

          present :beacon, beacon
        end
      end
    end
  end
end